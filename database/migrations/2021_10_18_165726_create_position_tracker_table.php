<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionTrackerTable extends Migration
{
    public function up()
    {
        $table_name = config('hermes.position_tracker_table_name');

        Schema::create($table_name, function(Blueprint $table) {
            $table->increments('id');
            $table->string('stream_id');
            $table->string('listener_id');
            $table->string('last_item_id');
            $table->unique(['stream_id', 'listener_id']);
            $table->timestamps();
        });
    }
}
