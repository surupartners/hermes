<?php

namespace Hermes;

use Illuminate\Container\Container;
use Hermes\Event\{
    ListenerId,
    ListenerRegistry,
};
use Hermes\Stream\{
    Config\StreamsConfig,
    StreamId,
};
use Hermes\Command\{
    RunStreamProcessor,
    RunStreamProcessors,
};
use Hermes\StreamEvent\{
    TransformationConfig,
    Transformer,
};
use Illuminate\Contracts\Foundation\Application;
use Hermes\Transaction\{
    LaravelTransactionManager,
    TransactionManager,
};
use function Functional\map;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/hermes.example.php' => config_path('hermes.php'),
            __DIR__ . '/../../config/stream_item_to_event_transformations.example.php' =>
                app_path('hermes_stream_item_to_event_transformations.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                RunStreamProcessors::class,
                RunStreamProcessor::class,
            ]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/hermes.php', 'hermes');

        $this->app->bind(TransactionManager::class, LaravelTransactionManager::class);
        $this->app->singleton(Transformer::class, function(Application $app) : Transformer {
            /** @var TransformationConfig $transformation_config */
            $transformation_config = $this->app->make(TransformationConfig::class);
            $transformations = $transformation_config->configuredTransformations();

            $transformer = new Transformer;
            foreach ($transformations as $transformation) {
                $transformer->registerTransformation($transformation);
            }

            return $transformer;
        });

        $this->app->singleton(ListenerRegistry::class, function(Application $app) : ListenerRegistry {
            $listener_registry = new ListenerRegistry($app->make(StreamsConfig::class), $app);

            // Register event listeners defined in config files
            foreach (config('hermes.event_listeners') as $listener_config) {
                if ( ! array_key_exists('class', $listener_config)) {
                    throw new \Exception('Configured event listeners must provide a "class" value.');
                }

                if ( ! array_key_exists('listens_for_events_from_streams', $listener_config)) {
                    throw new \Exception(
                        'Configured event listeners must provide a "listens_for_events_from_streams" value.'
                    );
                }

                $listener_registry->register(
                    new ListenerId($listener_config['class']),
                    fn(Container $app) => $app->make($listener_config['class']),
                    ...map(
                        $listener_config['listens_for_events_from_streams'],
                        fn(string $stream_id) => new StreamId($stream_id),
                    ),
                );
            }

            return $listener_registry;
        });
    }
}
