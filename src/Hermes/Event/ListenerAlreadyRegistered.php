<?php

namespace Hermes\Event;

class ListenerAlreadyRegistered extends \Exception
{
    public function __construct(ListenerId $listener_id)
    {
        parent::__construct("Listener {$listener_id} is already registered.");
    }
}
