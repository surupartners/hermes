<?php

namespace Hermes\Event;

class UnableToResolveListener extends \Exception
{
    public function __construct(ListenerId $listener_id, $previous_exception)
    {
        parent::__construct("Unable to resolve listener \"$listener_id\".", 0, $previous_exception);
    }
}
