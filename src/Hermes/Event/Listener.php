<?php

namespace Hermes\Event;

interface Listener
{
    function id() : ListenerId;

    function hear($event) : void;
}
