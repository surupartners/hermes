<?php

namespace Hermes\Event;

use Hermes\Stream\Config\StreamsConfig;
use Hermes\Stream\StreamId;
use Illuminate\Container\Container;
use function Functional\{
    first,
    map,
    some,
    unique,
};

class ListenerRegistry
{
    private array $listener_registrations = [];

    public function __construct(
        private StreamsConfig $streams_config,
        private Container $app,
    ) {}

    public function register(
        Listener | ListenerId $listener,
        callable $constructor,
        string | StreamId ...$events_or_streams
    ) : void {
        $id = $listener;
        if ($listener instanceof Listener) $id = $listener->id();

        if ($this->hasListener($id)) throw new ListenerAlreadyRegistered($id);

        $this->listener_registrations[] = [
            'listener_id' => $id,
            'constructor' => $constructor,
            'streams' => unique(map($events_or_streams, function(string | StreamId $event_or_stream) : StreamId {
                if ($event_or_stream instanceof StreamId) return $event_or_stream;

                return $this->streams_config->originatingStreamFor($event_or_stream);
            }), null, false),
        ];
    }

    public function hasListener(ListenerId $id) : bool
    {
        return some($this->listener_registrations, $this->registrationMatcher($id));
    }

    public function find(ListenerId $id) : Listener
    {
        $registration = first($this->listener_registrations, $this->registrationMatcher($id));

        if (is_null($registration)) throw new UnknownListener($id);

        return $registration['constructor']($this->app);
    }

    public function streamsListenerIsInterestedIn(ListenerId $id) : array
    {
        $registration = first($this->listener_registrations, $this->registrationMatcher($id));

        if (is_null($registration)) throw new UnknownListener($id);

        return $registration['streams'];
    }

    public function allIds() : array
    {
        return map($this->listener_registrations, fn(array $registration) => $registration['listener_id']);
    }

    private function registrationMatcher(ListenerId $id) : \Closure
    {
        return fn(array $registration) => $registration['listener_id']->equals($id);
    }
}
