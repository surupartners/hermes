<?php

namespace Hermes\Event;

class UnknownListener extends \Exception
{
    public function __construct(ListenerId $listener_id, \Exception $previous_exeception = null)
    {
        parent::__construct("Unable to find listener \"$listener_id\".", 0, $previous_exeception);
    }
}
