<?php

namespace Hermes\StreamEvent;

use Closure;

class InvalidTransformation extends \Exception
{
    private Closure $transformation;

    public function __construct(callable $transformation)
    {
        $this->transformation = Closure::fromCallable($transformation);

        parent::__construct('Transformations declare a non-null return type.');
    }

    public function transformation() : Closure
    {
        return $this->transformation;
    }
}
