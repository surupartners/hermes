<?php

namespace Hermes\StreamEvent;

use Closure;
use Hermes\Stream\StreamItem;
use ReflectionException;
use ReflectionFunction;

class Transformation
{
    private Closure $matching_predicate;
    private Closure $transformation;
    private string $transforms_to_type;

    /**
     * @throws ReflectionException
     * @throws InvalidTransformation
     */
    public function __construct(callable $matching_predicate, callable $transformation)
    {
        $this->setMatchingPredicate($matching_predicate);
        $this->setTransformation($transformation);
    }

    public static function wrap(self $existing_transformation, callable $matching_predicate)
    {
        return new self(
            function(StreamItem $stream_item) use ($matching_predicate, $existing_transformation) : bool {
                return $matching_predicate($stream_item, [$existing_transformation, 'matches']);
            },
            $existing_transformation->transformation,
        );
    }

    public function matches(StreamItem $stream_item) : bool
    {
        return $this->matching_predicate->call($this, $stream_item);
    }

    public function transform(StreamItem $stream_item) : mixed
    {
        try {
            return $this->transformation->call($this, $stream_item);
        } catch (\Exception $e) {
            throw new FailedToTransformStreamItem($stream_item, $this->transformation, $e);
        }
    }

    public function transformsTo() : string
    {
        return $this->transforms_to_type;
    }

    private function setMatchingPredicate(callable $matching_predicate) : void
    {
        $this->matching_predicate = Closure::fromCallable($matching_predicate);
    }

    /**
     * @throws InvalidTransformation
     * @throws ReflectionException
     */
    private function setTransformation(callable $transformation) : void
    {
        $transformation = Closure::fromCallable($transformation);

        $return_type = (new ReflectionFunction($transformation))->getReturnType();

        if ( is_null($return_type) || $return_type->allowsNull()) {
            throw new InvalidTransformation($transformation);
        }

        $this->transforms_to_type = $return_type->getName();
        $this->transformation = $transformation;
    }
}
