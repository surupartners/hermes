<?php

namespace Hermes\StreamEvent;

use Hermes\Stream\StreamItem;
use function Functional\first;

class TransformationList implements \IteratorAggregate
{
    /** @var Transformation[] */
    private array $transformations;

    public function __construct(Transformation ...$transformations)
    {
        $this->transformations = $transformations;
    }

    public function pushToStart(Transformation $transformation) : self
    {
        return new self($transformation, ...$this->transformations);
    }

    public function firstMatching(StreamItem $item) : ? Transformation
    {
        return first($this->transformations, fn(Transformation $transformation) => $transformation->matches($item));
    }

    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->transformations);
    }
}
