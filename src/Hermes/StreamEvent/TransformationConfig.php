<?php

namespace Hermes\StreamEvent;

use Hermes\Stream\{
    StreamId,
    StreamItem,
};
use Illuminate\Config\Repository;
use Illuminate\Log\Logger;
use function Functional\{
    first,
    map,
};

class TransformationConfig
{
    private Logger $log;

    private TransformationList $configured_transformations;

    public function __construct(Repository $laravel_config, Logger $log)
    {
        $this->log = $log;

        $transformations_registration_paths = $laravel_config->get(
            'hermes.stream_item_transformations_registration_path'
        );

        # May be a single configured path or an array of paths
        if ( ! is_array($transformations_registration_paths)) {
            $transformations_registration_paths = [$transformations_registration_paths];
        }

        $this->validatePaths(...$transformations_registration_paths);

        $transformations_by_registration_file = map(
            $transformations_registration_paths,
            fn(string $path) => require $path,
        );

        $transformation_configurations = array_merge(...map(
            $transformations_by_registration_file,
            fn(array $file_transformations) => array_reverse($file_transformations),
        ));

        $this->validateConfigurations(...$transformation_configurations);

        $configured_transformations = new TransformationList(...map(
            $transformation_configurations,
            fn(array $transformation) => new Transformation(
                $transformation['matching_predicate'],
                $transformation['transformation']
            ),
        ));

        // Automatically injects a check for the correct originating stream into all stream item to event
        // transformations.
        // @improvement This could do with a bit of a refactor
        $streams_events = $laravel_config->get('hermes.streams');

        $configured_transformations = new TransformationList(...map(
            $configured_transformations,
            function(Transformation $transformation) use ($streams_events) : Transformation {
                $transformation_stream = first($streams_events, fn(array $stream_events) => in_array(
                    $transformation->transformsTo(), $stream_events['events'],
                ));

                if (is_null($transformation_stream)) {
                    $transformation_stream_id = null;

                    $this->log->warning(
                        'There is a stream item to event transformation configured for "' .
                        "{$transformation->transformsTo()}\" but no stream provides it. "
                    );
                } else{
                    $transformation_stream_id = new StreamId($transformation_stream['class']);
                }

                return Transformation::wrap($transformation, function(
                    StreamItem $stream_item,
                    callable $existing_matching_predicate,
                ) use ($transformation_stream_id) : bool {
                    return ! is_null($transformation_stream_id)
                        && $stream_item->fromStream(new StreamId($transformation_stream_id))
                        && $existing_matching_predicate($stream_item);
                });
            },
        ));

        $this->configured_transformations = $configured_transformations;
    }

    public function configuredTransformations() : TransformationList
    {
        return $this->configured_transformations;
    }

    private function validatePaths(string ...$paths) : void
    {
        foreach ($paths as $path) {
            if ( ! is_string($path)) {
                throw new \Exception('Stream item to event transformation registration file paths must be strings.');
            }

            if ( ! file_exists($path)) {
                throw new \Exception("Unable to find stream item to event transformation registration file '{$path}'.");
            }
        }
    }

    private function validateConfigurations(array ...$transformation_configurations) : void
    {
        if ($transformation_configurations === []) {
            $this->log->warning('No registered stream item to event transformations.');
        }

        foreach ($transformation_configurations as $transformation_configuration) {
            if ( ! array_key_exists('matching_predicate', $transformation_configuration)) {
                throw new \Exception(
                    'Stream item to event transformation registrations must provide a \'matching_predicate\' value.'
                );
            }

            if ( ! array_key_exists('transformation', $transformation_configuration)) {
                throw new \Exception(
                    'Stream item to event transformation registrations must provide a \'transformation\' value.'
                );
            }
        }
    }
}
