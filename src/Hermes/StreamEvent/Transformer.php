<?php

namespace Hermes\StreamEvent;

use Hermes\Stream\StreamItem;

class Transformer
{
    private TransformationList $transformations;

    public function __construct()
    {
        $this->transformations = new TransformationList;
    }

    public function registerTransformation(Transformation $transformation) : void
    {
        $this->transformations = $this->transformations->pushToStart($transformation);
    }

    /** @throws FailedToTransformStreamItem */
    public function transform(StreamItem $item) : mixed
    {
        $transformation = $this->transformations->firstMatching($item);

        if (is_null($transformation)) return null;

        return $transformation->transform($item);
    }
}
