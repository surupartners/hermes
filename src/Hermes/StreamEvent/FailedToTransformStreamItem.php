<?php

namespace Hermes\StreamEvent;

use Closure;
use Hermes\Stream\StreamItem;

class FailedToTransformStreamItem extends \Exception
{
    private StreamItem $item;
    private Closure $transformation;

    public function __construct(StreamItem $item, callable $transformation, \Exception $previous_exception)
    {
        $this->item = $item;
        $this->transformation = Closure::fromCallable($transformation);

        parent::__construct(
            "Failed to transform stream item to {$this->transformationReturnType()}",
            0,
            $previous_exception
        );
    }

    public function streamItem(): StreamItem
    {
        return $this->item;
    }

    public function transformation() : Closure
    {
        return $this->transformation;
    }

    private function transformationReturnType() : string
    {
        return (new \ReflectionFunction($this->transformation))->getReturnType();
    }
}
