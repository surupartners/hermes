<?php

namespace Hermes\Monitoring\Health;

use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class ActivityCheck extends Check
{
    /**
     * @throws Exception
     */
    public function run() : Result
    {
        $statesTable = config('hermes.stream_processor_states_table_name');

        $result = Result::make();

        $processors = DB::table($statesTable)->get();

        $staleProcesses = [];
        foreach ($processors as $processor) {
            if (now()->subHours(24)->gt(Carbon::parse($processor->updated_at) )) {
                $staleProcesses[] = json_encode([
                    'stream_id' => $processor->stream_id,
                    'listener_id' => $processor->listener_id,
                    'state' => $processor->state,
                    'remarks' => $processor->remarks,
                    'updated_at' => $processor->updated_at,
                ]);
            }
        }

        if ( ! blank($staleProcesses)) {
            $result->meta($staleProcesses);
            return $result->failed();
        }

        $result->ok();
        return $result;
    }
}
