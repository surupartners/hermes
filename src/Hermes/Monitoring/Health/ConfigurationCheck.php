<?php

namespace Hermes\Monitoring\Health;

use Hermes\Stream\AvailableStreamProcessors;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class ConfigurationCheck extends Check
{
    protected ? string $name = 'Hermes Configuration';

    public function run() : Result
    {
        $result = Result::make();

        if (app(AvailableStreamProcessors::class)->isEmpty()) {
            return $result->warning('There are no configured stream processors.');
        }

        return $result->ok();
    }
}
