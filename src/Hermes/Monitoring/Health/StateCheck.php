<?php

namespace Hermes\Monitoring\Health;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class StateCheck extends Check
{
    public function run() : Result
    {
        $statesTable = config('hermes.stream_processor_states_table_name');

        $result = Result::make();
        $dead = DB::table($statesTable)
            ->where('state', 'Dead')->get();

        if (($deadCount = $dead->count()) > 0) {
            $result->failed("There are dead processors ($deadCount)");
            $result->meta($this->buildMetaData($dead));
            return $result;
        }

        $undead = DB::table($statesTable)
            ->where('state', '!=', 'Running')
            ->where('state', '!=', 'Dead')
            ->get();
        if (($undeadCount = $undead->count()) > 0) {
            $result->warning("There are processors not fully running ($undeadCount)");
            $result->meta($this->buildMetaData($undead));
            return $result;
        }

        $result->ok();
        return $result;
    }

    //
    // HELPER FUNCTIONS
    //

    private function buildMetaData(Collection $processorStates) : array
    {
        $meta = [];
        foreach ($processorStates as $processorState) {
            $meta[] = json_encode([
                'stream_id' => $processorState->stream_id,
                'listener_id' => $processorState->listener_id,
                'state' => $processorState->state,
                'remark' => $processorState->remarks,
            ]);
        }
        return $meta;
    }
}
