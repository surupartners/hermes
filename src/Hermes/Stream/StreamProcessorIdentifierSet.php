<?php

namespace Hermes\Stream;

use function Functional\filter;
use function Functional\some;

class StreamProcessorIdentifierSet implements \IteratorAggregate, \Countable
{
    /** @var StreamProcessorIdentifier[] */
    private array $identifiers;

    public function __construct(StreamProcessorIdentifier ...$identifiers)
    {
        $this->identifiers = $identifiers;
    }

    public function isEmpty() : bool
    {
        return $this->identifiers == [];
    }

    public function excluding(self $excludes) : self
    {
        return new self(...filter(
            $this->identifiers,
            fn(StreamProcessorIdentifier $identifier) => ! $excludes->contains($identifier),
        ));
    }

    public function intersecting(self $other_set) : self
    {
        return new self(...filter(
            $this->identifiers,
            fn(StreamProcessorIdentifier $identifier) => $other_set->contains($identifier),
        ));
    }

    public function getIterator() : \Traversable
    {
        return new \ArrayIterator($this->identifiers);
    }

    public function count() : int
    {
        return count($this->identifiers);
    }

    private function contains(StreamProcessorIdentifier $candidate_identifier) : bool
    {
        return some(
            $this->identifiers,
            fn(StreamProcessorIdentifier $existing_identifier) => $existing_identifier->equals($candidate_identifier),
        );
    }
}
