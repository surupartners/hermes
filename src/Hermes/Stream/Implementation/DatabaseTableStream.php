<?php

namespace Hermes\Stream\Implementation;

use Hermes\Stream\Stream;
use Hermes\Stream\StreamId;
use Hermes\Stream\StreamItem;
use Hermes\Stream\StreamItemId;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Log\Logger;

class DatabaseTableStream implements Stream
{
    private \Closure $item_payload_extractor;
    private \Closure $constraint;

    public function __construct(
        private string $table,
        private string $incrementing_column,
        callable $item_payload_extractor,
        callable $constraint,
        private Connection $database,
        private Logger $log,
    ) {
        $this->item_payload_extractor = \Closure::fromCallable($item_payload_extractor);
        $this->constraint = \Closure::fromCallable($constraint);
    }

    public function id() : StreamId
    {
        return new StreamId(static::class);
    }

    public function firstItem() : ? StreamItem
    {
        $this->log->debug('Fetching first matching item.', [
            'table' => $this->table,
            'stream' => $this->id(),
        ]);

        return $this->getItem();
    }

    public function itemFollowing(StreamItemId $item_id) : ? StreamItem
    {
        $this->log->debug('Fetching next matching item.', [
            'previous_item' => $item_id,
            'table' => $this->table,
            'stream' => $this->id(),
        ]);

        return $this->getItem(function(Builder $query) use ($item_id) : Builder {
            return $query->where(
                $this->incrementing_column,
                '>',
                function(Builder $query) use ($item_id) : Builder {
                    return $query
                        ->select($this->incrementing_column)
                        ->from($this->table)
                        ->where('id', (string) $item_id)
                        ->limit(1);
                },
            );
        });
    }

    private function getItem(callable ...$constraints) : ? StreamItem
    {
        $query = $this->database
            ->table($this->table)
            ->orderBy($this->incrementing_column);

        $constraints[] = $this->constraint;
        foreach ($constraints as $constraint) {
            $query = $constraint($query);
        }

        /*
         * Shared lock is acquired to deal with a situation where transaction A writes an event to the table before
         * transaction B but commits after. In such a case a row with a lower `incrementing_column` value will appear
         * after a row with a higher value and may be missed by Hermes.
         */
        $row = $query->sharedLock()->first();

        if (is_null($row)) { return null; }

        // Need to reassign like this or PHP thinks I'm calling a method
        $item_payload_extractor = $this->item_payload_extractor;
        return new StreamItem(
            new StreamItemId($row->id),
            $this->id(),
            $item_payload_extractor($row),
        );
    }
}
