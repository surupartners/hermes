<?php

namespace Hermes\Stream\Implementation;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Log\Logger;

class MessagesTableStream extends DatabaseTableStream
{
    public function __construct(string $topic, Connection $database, Logger $log)
    {
        parent::__construct(
            'messages',
            'id',
            fn(\stdClass $row) => $row->payload,
            function(Builder $query) use ($topic) : Builder {
                return $query->where('topic', $topic);
            },
            $database,
            $log,
        );
    }
}
