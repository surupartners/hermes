<?php

namespace Hermes\Stream\Implementation;

use Illuminate\Database\Connection;
use Illuminate\Log\Logger;

class InternalEventsTableStream extends DatabaseTableStream
{
    public function __construct(string $events_table, Connection $database, Logger $log)
    {
        parent::__construct(
            $events_table,
            'id',
            'Functional\id',
            'Functional\id',
            $database,
            $log,
        );
    }
}
