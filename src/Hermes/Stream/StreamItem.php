<?php

namespace Hermes\Stream;

class StreamItem
{
    private StreamItemId $id;
    private StreamId $stream_id;
    private mixed $payload;

    public function __construct(StreamItemId $id, StreamId $stream_id, mixed $payload)
    {
        $this->id = $id;
        $this->stream_id = $stream_id;
        $this->payload = $payload;
    }

    public function id() : StreamItemId
    {
        return $this->id;
    }

    public function streamId() : StreamId
    {
        return $this->stream_id;
    }

    public function fromStream(StreamId $stream_id) : bool
    {
        return $this->stream_id->equals($stream_id);
    }

    public function payload() : mixed
    {
        return $this->payload;
    }
}
