<?php

namespace Hermes\Stream\Config;

use Hermes\Stream\StreamId;
use Illuminate\Config\Repository;
use function Functional\every;
use function Functional\first;

class StreamConfig
{
    private string $config_namespace = 'hermes';
    private array $stream_config;
    private Repository $laravel_config;

    public function __construct(array $stream_config, Repository $laravel_config)
    {
        $this->validateEvents($stream_config);

        $this->stream_config = $stream_config;
        $this->laravel_config = $laravel_config;
    }

    public function providesEvents() : array
    {
        return $this->stream_config['events'];
    }

    public function provides(string $event) : bool
    {
        return in_array($event, $this->providesEvents());
    }

    public function recheckDelay() : ? RecheckDelay
    {
        if ($this->hasConfigValue('recheck_delay_after_end_of_stream_in_seconds')) {
            return RecheckDelay::fromSeconds($this->getConfigValue('recheck_delay_after_end_of_stream_in_seconds'));
        }

        return null;
    }

    public function processingDelay() : ? ProcessingDelay
    {
        if ($this->hasConfigValue('processing_delay_between_stream_items_in_seconds')) {
            return ProcessingDelay::fromSeconds($this->getConfigValue('processing_delay_between_stream_items_in_seconds'));
        }

        return null;
    }

    // Intended to be package private
    public function hasConfigValue(string $value_name) : bool
    {
        return ! is_null($this->getConfigValue($value_name));
    }

    // Intended to be package-private
    public function getConfigValue(string $value_name) : mixed
    {
        if (array_key_exists($value_name, $this->stream_config)) {
            return $this->stream_config[$value_name];
        }

        if ($this->laravel_config->has("{$this->config_namespace}.{$value_name}")) {
            return $this->laravel_config->get("{$this->config_namespace}.{$value_name}");
        }

        return null;
    }

    private function validateEvents(array $stream_config) : void
    {
        if ([] === $stream_config['events']) {
            throw new \Exception('All configured streams must have a non-empty list of provided events.');
        }
    }
}
