<?php

namespace Hermes\Stream\Config;

use Hermes\Event\ListenerId;
use Hermes\Process\Config\{
    ImmediateRestartLimit,
    ImmediateRestartLimitWindow,
    RestartBackOffLimit,
    RestartBackOffLimitWindow,
    RestartBackOffPeriod,
};
use Hermes\Stream\StreamId;
use Illuminate\Config\Repository;
use function Functional\{
    every,
    first,
};

class StreamProcessorConfig
{
    private string $config_namespace = 'hermes';
    private array $stream_processor_config;
    private StreamConfig $stream_config;

    public function __construct(
        Repository $laravel_config,
        StreamsConfig $streams_config,
        StreamId $stream_id,
        ListenerId $listener_id
    ) {
        $this->stream_processor_config = $this->getStreamProcessorConfig($laravel_config, $stream_id, $listener_id);
        $this->stream_config = $streams_config->for($stream_id);
    }

    public function recheckDelay() : RecheckDelay
    {
        return RecheckDelay::fromSeconds($this->getConfigValue('recheck_delay_after_end_of_stream_in_seconds'));
    }

    public function processingDelay() : ProcessingDelay
    {
        return ProcessingDelay::fromSeconds($this->getConfigValue('processing_delay_between_stream_items_in_seconds'));
    }

    public function immediateRestartLimit() : ImmediateRestartLimit
    {
        return new ImmediateRestartLimit($this->getConfigValue('immediate_restart_limit'));
    }

    public function immediateRestartLimitWindow() : ImmediateRestartLimitWindow
    {
        return ImmediateRestartLimitWindow::fromSeconds(
            $this->getConfigValue('immediate_restart_limit_window_in_seconds'),
        );
    }

    public function restartBackOffPeriod() : RestartBackOffPeriod
    {
        return RestartBackOffPeriod::fromHours($this->getConfigValue('restart_back_off_period_in_hours'));
    }

    public function restartBackOffLimit() : RestartBackOffLimit
    {
        return new RestartBackOffLimit($this->getConfigValue('restart_back_off_limit'));
    }

    public function restartBackOffLimitWindow() : RestartBackOffLimitWindow
    {
        return RestartBackOffLimitWindow::fromHours($this->getConfigValue('restart_back_off_limit_window_in_hours'));
    }

    /** @throws \Exception */
    private function getConfigValue(string $value_name) : mixed
    {
        if (array_key_exists($value_name, $this->stream_processor_config)) {
            return $this->stream_processor_config[$value_name];
        }

        if ($this->stream_config->hasConfigValue($value_name)) {
            return $this->stream_config->getConfigValue($value_name);
        }

        throw new \Exception("No '$value_name' configuration found.");
    }

    /** @throws \Exception */
    private function getStreamProcessorConfig(
        Repository $laravel_config,
        StreamId $stream_id,
        ListenerId $listener_id
    ) : array {
        $stream_processors_config = $laravel_config->get("$this->config_namespace.stream_processors") ?? [];

        if ( ! every($stream_processors_config, function(array $processor_config) : bool {
            return array_key_exists('stream', $processor_config)
                && array_key_exists('listener', $processor_config);
        })) {
            throw new \Exception('All configured stream processors must have a stream and a listener.');
        }

        return first(
            $stream_processors_config,
            function(array $processor_config) use ($stream_id, $listener_id) : bool {
                return $stream_id->equals(new StreamId($processor_config['stream']))
                    && $listener_id->equals(new ListenerId($processor_config['listener']));
            },
        ) ?? [];
    }
}
