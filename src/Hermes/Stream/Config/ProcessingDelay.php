<?php

namespace Hermes\Stream\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class ProcessingDelay implements TimePeriod
{
    use TimePeriodTrait;
}
