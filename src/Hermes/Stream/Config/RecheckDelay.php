<?php

namespace Hermes\Stream\Config;

use Hermes\Common\TimePeriod;
use Hermes\Common\TimePeriodTrait;

class RecheckDelay implements TimePeriod
{
    use TimePeriodTrait;
}
