<?php

namespace Hermes\Stream\Config;

use Hermes\Stream\StreamId;
use Hermes\Stream\UnknownStream;
use Illuminate\Config\Repository;
use function Functional\every;

class StreamsConfig
{
    /** @var StreamConfig[] */
    private array $streams;

    public function __construct(Repository $laravel_config)
    {
        $streams_config = $laravel_config->get('hermes.streams');

        $this->validateStreams(...$streams_config);
        $this->validateEvents(...$streams_config);

        foreach ($streams_config as $stream_config) {
            $stream_id = $stream_config['class'];

            $this->streams[$stream_id] = new StreamConfig($stream_config, $laravel_config);
        }
    }

    public function for(StreamId $stream_id) : StreamConfig
    {
        if ( ! array_key_exists((string) $stream_id, $this->streams)) {
            throw new UnknownStream($stream_id);
        }

        return $this->streams[(string) $stream_id];
    }

    public function hasConfigFor(StreamId $stream_id) : bool
    {
        return array_key_exists((string) $stream_id, $this->streams);
    }

    public function originatingStreamFor(string $event) : StreamId
    {
        foreach ($this->streams as $stream_id => $stream_config) {
            if ($stream_config->provides($event)) {
                return new StreamId($stream_id);
            }
        }

        throw new \Exception("No stream provides the event \"{$event}\".");
    }

    private function validateStreams(array ...$streams_config) : void
    {
        if ( ! every($streams_config, function(array $stream_config) : bool {
            return array_key_exists('class', $stream_config);
        })) {
            throw new \Exception('All configured streams must have a class value.');
        }

        $already_configured_streams = [];
        foreach ($streams_config as $stream_config) {
            if (in_array($stream_config['class'], $already_configured_streams)) {
                throw new \Exception("The stream '{$stream_config['class']}' is configured more than once.");
            }

            $already_configured_streams[] = $stream_config['class'];
        }
    }

    private function validateEvents(array ...$streams_config) : void
    {
        $already_provided_events = [];
        foreach ($streams_config as $stream_config) {
            $reprovided_events = array_intersect($stream_config['events'], $already_provided_events);

            if ($reprovided_events !== []) {
                throw new \Exception(
                    'The events "' . implode(', ', $reprovided_events) . '" provided by "' . $stream_config['class'] .
                    '" are already provided by other streams.'
                );
            }

            $already_provided_events = array_merge($already_provided_events, $stream_config['events']);
        }
    }
}
