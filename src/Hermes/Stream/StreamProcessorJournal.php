<?php

namespace Hermes\Stream;

use Illuminate\Log\Logger;

class StreamProcessorJournal
{
    public function __construct(
        private Logger $log,
        private ProcessorStateTable $state_table,
    ) {}

    public function recordEntry(StreamProcessorIdentifier $processor, string $state, string $remarks) : void
    {
        $this->log->info("Stream processor '{$processor}' is '{$state}'. Remarks: '{$remarks}'.");

        $this->state_table->writeNewState(
            $processor->streamId(),
            $processor->listenerId(),
            $state,
            $remarks
        );
    }

    public function clear() : void
    {
        $this->state_table->truncate();
    }
}
