<?php

namespace Hermes\Stream;

use Throwable;

class UnknownStream extends \Exception
{
    public function __construct(StreamId $stream_id, \Exception $previous_exeception = null)
    {
        parent::__construct("Unable to find stream \"$stream_id\".", 0, $previous_exeception);
    }
}
