<?php

namespace Hermes\Stream;

use Hermes\Event\{
    ListenerId,
    ListenerRegistry,
};
use Hermes\Stream\Config\StreamsConfig;
use Illuminate\Log\Logger;
use function Functional\{
    flatten,
    map,
    some,
};

class AvailableStreamProcessors
{
    private StreamProcessorIdentifierSet $available_stream_processors;

    public function __construct(ListenerRegistry $listener_registry, StreamsConfig $streams_config, Logger $log)
    {
        if ($listener_registry->allIds() == []) {
            $log->warning('No configured event listeners.');
        }

        $this->available_stream_processors = new StreamProcessorIdentifierSet(...flatten(
            $this->matchStreamsToInterestedListeners($listener_registry, $streams_config, $log)
        ));
    }

    public function isEmpty() : bool
    {
        return $this->available_stream_processors->isEmpty();
    }

    public function all() : StreamProcessorIdentifierSet
    {
        return $this->available_stream_processors;
    }

    public function excluding(StreamProcessorIdentifierSet $excludes) : StreamProcessorIdentifierSet
    {
        return $this->available_stream_processors->excluding($excludes);
    }

    private function matchStreamsToInterestedListeners(
        ListenerRegistry $listener_registry,
        StreamsConfig $streams_config,
        Logger $log,
    ) : array {

        return map($listener_registry->allIds(), function(ListenerId $listener_id) use (
            $listener_registry, $streams_config, $log,
        ) {
            $streams_of_interest = $listener_registry->streamsListenerIsInterestedIn($listener_id);

            if ($streams_of_interest == []) {
                $log->warning("Listener '{$listener_id}' doesn't listen for events from any streams.");
            }

            if (some($streams_of_interest, fn(StreamId $id) => ! $streams_config->hasConfigFor($id))) {
                throw new \Exception(
                    "Listener '{$listener_id}' configured to listen for events from an unconfigured stream."
                );
            }

            return map($streams_of_interest, fn(StreamId $stream_id) => new StreamProcessorIdentifier(
                $stream_id, $listener_id,
            ));
        });
    }
}
