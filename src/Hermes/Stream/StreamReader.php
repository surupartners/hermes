<?php

namespace Hermes\Stream;

use Hermes\Event\ListenerId;
use Hermes\Stream\PositionTracker\PositionTracker;

class StreamReader
{
    private Stream $stream;
    private ListenerId $listener_id;
    private PositionTracker $position_tracker;

    public function __construct(Stream $stream, ListenerId $listener_id, PositionTracker $position_tracker)
    {
        $this->stream = $stream;
        $this->listener_id = $listener_id;
        $this->position_tracker = $position_tracker;
    }

    public function nextItem() : ? StreamItem
    {
        $last_item_id = $this->position_tracker->getLastItemFor($this->stream->id(), $this->listener_id);

        if (is_null($last_item_id)) {
            $next_item = $this->stream->firstItem();
        } else {
            $next_item = $this->stream->itemFollowing($last_item_id);
        }

        if (is_null($next_item)) {
            return null;
        }

        $this->position_tracker->updateLastItemFor($this->stream->id(), $this->listener_id, $next_item->id());

        return $next_item;
    }
}
