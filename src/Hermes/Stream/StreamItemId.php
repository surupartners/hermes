<?php

namespace Hermes\Stream;

class StreamItemId implements \JsonSerializable
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function equals(self $comparator) : bool
    {
        return $this->id == $comparator->id;
    }

    public function __toString() : string
    {
        return $this->id;
    }

    public function jsonSerialize() : mixed
    {
        return (string) $this;
    }
}
