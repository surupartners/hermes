<?php

namespace Hermes\Stream;

use Hermes\Event\ListenerId;
use Illuminate\Config\Repository;
use Illuminate\Support\Facades\DB;

class ProcessorStateTable
{
    private string $table;

    public function __construct(Repository $laravel_config)
    {
        $this->table = $laravel_config->get('hermes.stream_processor_states_table_name');
    }

    public function truncate() : void
    {
        DB::table($this->table)->truncate();
    }

    public function writeNewState(
        StreamId $stream_id,
        ListenerId $listener_id,
        string $state,
        string $remarks,
    ) : void {
        DB::table($this->table)->updateOrInsert([
            'stream_id' => $stream_id,
            'listener_id' => $listener_id,
        ], [
            'state' => $state,
            'remarks' => $remarks,
            'updated_at' => DB::raw('NOW()'),
        ]);
    }
}
