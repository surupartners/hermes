<?php

namespace Hermes\Stream;

interface Stream
{
    function id() : StreamId;

    function firstItem() : ? StreamItem;

    function itemFollowing(StreamItemId $item_id) : ? StreamItem;
}
