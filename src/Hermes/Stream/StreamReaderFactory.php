<?php

namespace Hermes\Stream;

use Hermes\Event\ListenerId;
use Hermes\Stream\PositionTracker\PositionTracker;

class StreamReaderFactory
{
    private StreamRegistry $stream_registry;
    private PositionTracker $position_tracker;

    public function __construct(StreamRegistry $stream_registry, PositionTracker $position_tracker)
    {
        $this->stream_registry = $stream_registry;
        $this->position_tracker = $position_tracker;
    }

    public function build(StreamId $stream_id, ListenerId $listener_id) : StreamReader
    {
        return new StreamReader($this->stream_registry->find($stream_id), $listener_id, $this->position_tracker);
    }
}
