<?php

namespace Hermes\Stream\PositionTracker;

use Hermes\Event\ListenerId;
use Hermes\Stream\{
    StreamId,
    StreamItemId,
};

class PositionTracker
{
    public function getLastItemFor(StreamId $stream_id, ListenerId $listener_id) : ? StreamItemId
    {
        $marker = PositionMarker::where('stream_id', '=', $stream_id)
            ->where('listener_id', '=', $listener_id)
            ->lockForUpdate()
            ->first();

        return $marker?->last_item_id;
    }

    public function updateLastItemFor(StreamId $stream_id, $listener_id, StreamItemId $last_item_id) : void
    {
        $marker = PositionMarker::firstOrNew([
            'stream_id' => $stream_id,
            'listener_id' => $listener_id,
        ]);

        $marker->last_item_id = $last_item_id;
        $marker->save();
    }
}
