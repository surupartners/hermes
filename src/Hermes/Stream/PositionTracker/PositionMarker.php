<?php

namespace Hermes\Stream\PositionTracker;

use Hermes\Stream\StreamItemId;
use Illuminate\Database\Eloquent\Model;

class PositionMarker extends Model
{
    protected $guarded = [];

    public function getTable()
    {
        return config('hermes.position_tracker_table_name');
    }

    public function getLastItemIdAttribute($id)
    {
        return new StreamItemId($id);
    }
}
