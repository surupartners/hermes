<?php

namespace Hermes\Stream;

class UnableToResolveStream extends \Exception
{
    public function __construct(StreamId $stream_id, $previous_exception)
    {
        parent::__construct("Unable to resolve stream \"$stream_id\".", 0, $previous_exception);
    }
}
