<?php

namespace Hermes\Stream;

use Hermes\Event\ListenerId;
use Hermes\Stream\StreamId;

class StreamProcessorIdentifier
{
    private StreamId $stream_id;
    private ListenerId $listener_id;

    public function __construct(StreamId $stream_id, ListenerId $listener_id)
    {
        $this->stream_id = $stream_id;
        $this->listener_id = $listener_id;
    }

    public function streamId() : StreamId
    {
        return $this->stream_id;
    }

    public function listenerId() : ListenerId
    {
        return $this->listener_id;
    }

    public function equals(self $candidate) : bool
    {
        return $this->stream_id->equals($candidate->stream_id) && $this->listener_id->equals($candidate->listener_id);
    }

    public function __toString() : string
    {
        return $this->stream_id . ':' . $this->listener_id;
    }
}
