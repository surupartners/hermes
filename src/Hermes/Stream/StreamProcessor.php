<?php

namespace Hermes\Stream;

use Hermes\Stream\Config\RecheckDelay;
use Illuminate\Log\Logger;
use Hermes\Event\{
    Listener,
    ListenerId,
    ListenerRegistry,
};
use Hermes\StreamEvent\Transformer;
use Hermes\Transaction\TransactionManager;

class StreamProcessor
{
    private StreamReader $stream_reader;
    private Listener $listener;

    public function __construct(
        private StreamId $stream_id,
        private ListenerId $listener_id,
        private RecheckDelay $recheck_delay,
        StreamReaderFactory $stream_reader_factory,
        ListenerRegistry $listener_registry,
        private Transformer $transformer,
        private TransactionManager $transaction_manager,
        private Logger $log,
    ) {
        $this->stream_reader = $stream_reader_factory->build($stream_id, $listener_id);
        $this->listener = $listener_registry->find($listener_id);
    }

    public function processNextItem() : void
    {
        $this->log('Starting processNextItem()');

        while (true) {
            $processed_new_item = $this->attemptToProcessNextItem();

            if ($processed_new_item) {
                break;
            }

            $this->waitBeforeRecheckingForNewItem();
        }
    }

    function attemptToProcessNextItem() : bool
    {
        $processed_new_item = false;

        $this->transaction_manager->transaction(function() use (&$processed_new_item) {
            $this->log('Started transaction.');

            $stream_item = $this->attemptToFetchNextItem();

            if (is_null($stream_item)) return;
            $processed_new_item = true;

            $event = $this->attemptToTransformStreamItemToEvent($stream_item);

            if (is_null($event)) {
                $this->log('No transformation found for stream item so doing nothing with this item.', [
                    'stream_item' => $stream_item->id(),
                ]);
                return;
            }

            $this->passEventToListener($event);
        });

        return $processed_new_item;
    }

    function attemptToFetchNextItem() : ? StreamItem
    {
        $this->log('About to fetch next item...');

        $stream_item = $this->stream_reader->nextItem();

        return $stream_item;
    }

    function waitBeforeRecheckingForNewItem() : void
    {
        $this->log('No new stream item found. Waiting...', ['delay_in_seconds' => $this->recheck_delay->toSeconds()]);

        sleep($this->recheck_delay->toSeconds());
    }

    function attemptToTransformStreamItemToEvent(StreamItem $stream_item) : mixed
    {
        $this->log('Transforming stream item to event.', ['stream_item' => $stream_item->id()]);

        return $this->transformer->transform($stream_item);
    }

    function passEventToListener(mixed $event) : void
    {
        $this->log('Passing event to listener...');

        $this->listener->hear($event);

        $this->log('Listener has processed event. Finished with this event.');
    }

    private function log(string $message, array $additional_context = []) : void
    {
        $this->log->debug($message, [
            'stream' => $this->stream_id,
            'listener' => $this->listener_id,
            ...$additional_context,
        ]);
    }
}
