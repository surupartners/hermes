<?php

namespace Hermes\Stream;

use Illuminate\Support\Facades\App;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class StreamRegistry
{
    /**
     * @throws UnknownStream
     * @throws UnableToResolveStream
     */
    public function find(StreamId $id) : Stream
    {
        try {
            return App::make((string) $id);
        } catch (NotFoundExceptionInterface $e) {
            throw new UnknownStream($id, $e);
        } catch (ContainerExceptionInterface $e) {
            throw new UnableToResolveStream($id, $e);
        }
    }
}
