<?php

namespace Hermes\Common;

trait TimePeriodTrait
{
    private int $period_in_seconds;

    public function __construct(int $period_in_seconds)
    {
        $this->period_in_seconds = $period_in_seconds;
    }

    public static function fromSeconds(int $period_in_seconds) : self
    {
        return new self($period_in_seconds);
    }

    public static function fromMinutes(int $period_in_minutes) : self
    {
        return new self($period_in_minutes * 60);
    }

    public static function fromHours(int $period_in_hours) : self
    {
        return new self($period_in_hours * 3600);
    }

    public function toSeconds() : int
    {
        return $this->period_in_seconds;
    }

    public function equals(TimePeriod $candidate) : bool
    {
        return $this->toSeconds() == $candidate->toSeconds();
    }

    public function lessThan(TimePeriod $comparator) : bool
    {
        return $this->period_in_seconds < $comparator->period_in_seconds;
    }
}
