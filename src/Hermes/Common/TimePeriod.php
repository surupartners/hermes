<?php

namespace Hermes\Common;

interface TimePeriod
{
    static function fromSeconds(int $period_in_seconds) : self;

    static function fromMinutes(int $period_in_minutes) : self;

    static function fromHours(int $period_in_hours) : self;

    function toSeconds() : int;

    function equals(self $candidate) : bool;

    function lessThan(self $comparator) : bool;
}
