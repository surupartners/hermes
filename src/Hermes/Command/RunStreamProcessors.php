<?php

namespace Hermes\Command;

use Carbon\Carbon;
use Hermes\Event\ListenerId;
use Hermes\Stream\{
    AvailableStreamProcessors,
    Config\StreamProcessorConfig,
    StreamId,
    StreamProcessorIdentifier,
    StreamProcessorIdentifierSet,
    StreamProcessorJournal,
};
use Hermes\Process\{
    Config\Config,
    Config\GracefulTerminationTimeout,
    Babysitter,
    ProcessesReport,
    ProcessReport,
};
use Illuminate\Console\Command;
use Illuminate\Support\Facades\{
    App,
    Log,
};
use Symfony\Component\Console\{
    Command\SignalableCommandInterface,
    Output\ConsoleOutputInterface,
};
use Symfony\Component\Process\{
    Exception\LogicException,
    Exception\RuntimeException,
    Process,
};
use function Functional\{
    filter,
    first,
    map,
};

class RunStreamProcessors extends Command implements SignalableCommandInterface
{
    protected $signature = 'stream-processors:run '
        . '{--only= : A comma-separated list of  <stream id:listener id> pairs for which stream processors should be '
        . 'run. No other stream processors will be run.} '
        . '{--exclude= : A comma-separated list of <stream id:listener id> pairs for which stream processors should '
        . 'not be run. All other registered stream processors will be run.}';

    protected $description = 'Run all registered stream processors.';

    private bool $signaled_to_end = false;

    private $stdout;
    private $stderr;

    private GracefulTerminationTimeout $graceful_termination_timeout;

    /** @var Process[] */
    private array $children;

    public function handle(
        Config $config,
        AvailableStreamProcessors $available_processors,
        StreamProcessorJournal $stream_processor_journal,
    ) : int {
        $this->stdout = $this->getOutput();
        $this->stderr = $this->stdout;

        // Transparently pass through child output
        $process_output_handler = function(string $type, $buffer) {
            if ($type === Process::ERR) {
                $this->stderr->write($buffer);
                return;
            }

            $this->stdout->write($buffer);
        };
        $process_babysitter = new Babysitter($process_output_handler);

        if ($this->stdout instanceof ConsoleOutputInterface) {
            $this->stderr = $this->stdout->getErrorOutput();
        }

        $this->graceful_termination_timeout = $config->gracefulTerminationTimout();
        $monitoring_frequency = $config->monitoringFrequency();

        $stream_processors = $this->streamProcessorsToRun($available_processors);

        $stream_processor_journal->clear();

        foreach ($stream_processors as $stream_processor) {
            /** @var StreamProcessorIdentifier $stream_processor */

            $child = new Process([
                'php',
                'artisan',
                'stream-processors:run-one',
                $stream_processor->streamId(),
                $stream_processor->listenerId(),
            ], base_path());
            $this->children[] = [
                'id' => $stream_processor,
                'process' => $child,
            ];

            $child->start($process_output_handler);

            /** @var StreamProcessorConfig $stream_processor_config */
            $stream_processor_config = App::make(StreamProcessorConfig::class, [
                'stream_id' => $stream_processor->streamId(),
                'listener_id' => $stream_processor->listenerId(),
            ]);

            $process_babysitter->watch(
                $stream_processor,
                $child,
                $stream_processor_config->immediateRestartLimit(),
                $stream_processor_config->immediateRestartLimitWindow(),
                $stream_processor_config->restartBackOffPeriod(),
                $stream_processor_config->restartBackOffLimit(),
                $stream_processor_config->restartBackOffLimitWindow(),
            );
        }

        while (true) {
            sleep($monitoring_frequency->toSeconds());

            // TODO: handle process reports from babysitter
            $report = $process_babysitter->checkOnProcesses();

            $this->publishProcessorStates($report, $stream_processor_journal);
        }

        return 0;
    }

    public function getSubscribedSignals() : array
    {
        return [SIGTERM, SIGINT];
    }

    public function handleSignal(int $signal) : void
    {
        if ($this->signaled_to_end) {
            exit(2);
        }

        $this->signaled_to_end = true;

        $message = 'Attempting to gracefully stop stream processors...';
        if ($signal == SIGINT) {
            $this->stderr->writeln($message);
        }
        Log::info($message);

        $exit_code = 0;
        foreach ($this->children as $child) {
            /** @var StreamProcessorIdentifier $id */
            $id = $child['id'];
            /** @var Process $process */
            $process = $child['process'];

            try {
                $process->signal($signal);
            } catch (LogicException $e) {
                // Do nothing, the process isn't running
            } catch (RuntimeException $e) {
                $error_msg = "Unable to stop processor '$id' (PID: {$process->getPid()}):\n{$e->getMessage()}";

                if ($signal == SIGINT) {
                    $this->errorMsg($error_msg);
                }

                Log::error($error_msg);
                $exit_code = 1;
            }
        }

        $running_children = [];
        $start_time = Carbon::now();
        do {
            if ($this->graceful_termination_timeout->lessThan(GracefulTerminationTimeout::fromSeconds(
                Carbon::now()->diffInSeconds($start_time)
            ))) {
                $exit_code = 1;
                break;
            }

            sleep(1);

            $running_children = filter($this->children, fn(array $child) => $child['process']->isRunning());
        } while (count($running_children) > 0);

        foreach ($running_children as $child) {
            /** @var StreamProcessorIdentifier $id */
            $id = $child['id'];
            /** @var Process $process */
            $process = $child['process'];

            $warning = "Killing processor '$id' (PID: {$process->getPid()}).";

            if ($signal == SIGINT) {
                $this->stderr->writeln($warning);
            }
            Log::warning($warning);

            $process->signal(SIGKILL);
        }

        exit($exit_code);
    }

    private function publishProcessorStates(
        ProcessesReport $processes_report,
        StreamProcessorJournal $stream_processor_journal,
    ) : void {
        foreach ($processes_report as $process_report) {
            /** @var ProcessReport $process_report */

            // Find the stream processor running in the process
            $stream_processor = first(
                $this->children,
                fn(array $child) => $process_report->isFor((string) $child['id']),
            )['id'];

            $stream_processor_journal->recordEntry(
                $stream_processor,
                $process_report->health(),
                $process_report->remarks(),
            );
        }
    }

    private function streamProcessorsToRun(
        AvailableStreamProcessors $available_processors
    ) : StreamProcessorIdentifierSet {
        if ( ! is_null($this->option('only')) && ! is_null($this->option('exclude'))) {
            $this->errorMsg('Cannot provide both `--only` and `--exclude` options at the same time.');
            exit(1);
        }

        if ( ! is_null($this->option('only'))) {
            $only_processors = $this->wrapStreamProcessorArguments($this->option('only'), '--only');

            $unrecognised_processors = $only_processors->excluding($available_processors->all());
            if (count($unrecognised_processors) > 0) {
                foreach ($unrecognised_processors as $unrecognised_processor) {
                    $this->errorMsg("Processor '$unrecognised_processor' was not recognised.");
                }

                exit(1);
            }

            return $only_processors->intersecting($available_processors->all());
        }

        if ( ! is_null($this->option('exclude'))) {
            $exclude_processors = $this->wrapStreamProcessorArguments($this->option('exclude'), '--exclude');

            return $available_processors->excluding($exclude_processors);
        }

        return $available_processors->all();
    }

    private function errorMsg(string $msg) : void
    {
        $this->stderr->writeln("<bg=red>$msg</>");
    }

    private function wrapStreamProcessorArguments(string $args, string $option) : StreamProcessorIdentifierSet
    {
        $processors = explode(',', $args);

        return new StreamProcessorIdentifierSet(...map(
            $processors,
            function(string $processor) use ($option) : StreamProcessorIdentifier {
                $processor_parts = explode(':', $processor);

                if (count($processor_parts) != 2) {
                    $this->errorMsg(
                        "Arguments to '{$option}' must be of the form '<stream-id>:<listener-id>', " .
                        "'{$processor}' given."
                    );
                    exit(1);
                }

                return new StreamProcessorIdentifier(
                    new StreamId($processor_parts[0]),
                    new ListenerId($processor_parts[1]),
                );
            }
        ));
    }
}
