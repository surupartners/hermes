<?php

namespace Hermes\Command;

use Hermes\Event\ListenerId;
use Illuminate\Support\Facades\{
    App,
    Log,
};
use Hermes\Stream\{
    Config\ProcessingDelay,
    Config\RecheckDelay,
    Config\StreamProcessorConfig,
    StreamId,
    StreamProcessor,
};
use Illuminate\Console\Command;
use Symfony\Component\Console\{
    Command\SignalableCommandInterface,
    Output\ConsoleOutputInterface,
};

class RunStreamProcessor extends Command implements SignalableCommandInterface
{
    protected $signature = 'stream-processors:run-one {stream_id} {listener_id}';

    protected $description = 'Run the given stream processor';

    private bool $end_signal_received = false;
    private ? int $signal_received = null;

    public function handle() : int
    {
        $stream_id = new StreamId($this->argument('stream_id'));
        $listener_id = new ListenerId($this->argument('listener_id'));

        $stdout = $this->getOutput();
        $stderr = $stdout;

        if ($stdout instanceof ConsoleOutputInterface) {
            $stderr = $stdout->getErrorOutput();
        }

        /** @var StreamProcessorConfig $processor_config */
        $processor_config = App::make(StreamProcessorConfig::class, [
            'stream_id' => $stream_id,
            'listener_id' => $listener_id,
        ]);

        /** @var StreamProcessor $stream_processor */
        $stream_processor = App::makeWith(StreamProcessor::class, [
            'stream_id' => $stream_id,
            'listener_id' => $listener_id,
            'recheck_delay' => $processor_config->recheckDelay(),
        ]);

        Log::debug('Starting stream processor on stream "' . $stream_id . '" for listener "' . $listener_id . '".');

        $this->runStreamProcessor(
            $stream_processor,
            $processor_config->processingDelay(),
            $stream_id,
            $listener_id,
        );

        if ($this->signal_received == SIGINT) {
            $stderr->writeln("Successfully stopped processor for stream '$stream_id' and listener '$listener_id'.");
        }

        return 0;
    }

    public function getSubscribedSignals() : array
    {
        return [SIGTERM, SIGINT];
    }

    public function handleSignal(int $signal) : void
    {
        $this->end_signal_received = true;
        $this->signal_received = $signal;
    }

    private function runStreamProcessor(
        StreamProcessor $stream_processor,
        ProcessingDelay $processing_delay,
        StreamId $stream_id,
        ListenerId $listener_id,
    ) : void {
        while ( ! $this->end_signal_received) {
            Log::debug('About to process next item.', [
                'stream' => $stream_id,
                'listener' => $listener_id,
            ]);

            $stream_processor->processNextItem();

            Log::debug('Waiting to process next item', [
                'stream' => $stream_id,
                'listener' => $listener_id,
                'delay_in_seconds' => $processing_delay->toSeconds(),
            ]);

            sleep($processing_delay->toSeconds());
        }

        Log::debug('Ending stream processor due to signal.', [
            'stream' => $stream_id,
            'listener' => $listener_id,
            'signal' => $this->signal_received,
        ]);
    }
}
