<?php

namespace Hermes\Transaction;

interface TransactionManager
{
    function transaction(callable $transaction) : void;
}
