<?php

namespace Hermes\Transaction;

use Illuminate\Support\Facades\DB;

class LaravelTransactionManager implements TransactionManager
{
    function transaction(callable $transaction) : void
    {
        DB::transaction($transaction);
    }
}
