<?php

namespace Hermes\Process\Health;

class Running implements Health
{
    public function __toString() : string
    {
        return 'Running';
    }
}
