<?php

namespace Hermes\Process\Health;

class Dead implements Health
{
    public function __toString() : string
    {
        return 'Dead';
    }
}
