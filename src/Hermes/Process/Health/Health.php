<?php

namespace Hermes\Process\Health;

interface Health
{
    function __toString() : string;
}
