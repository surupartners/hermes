<?php

namespace Hermes\Process\Health;

class BackingOffForRestart implements Health
{
    public function __toString() : string
    {
        return 'Backing off for restart';
    }
}
