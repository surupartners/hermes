<?php

namespace Hermes\Process\Health;

class AttemptingRestart implements Health
{
    public function __toString() : string
    {
        return 'Attempting restart';
    }
}
