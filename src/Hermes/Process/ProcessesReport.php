<?php

namespace Hermes\Process;

class ProcessesReport implements \IteratorAggregate
{
    /** @var ProcessReport[] */
    private array $reports;

    public function __construct(ProcessReport ...$reports)
    {
        $this->reports = $reports;
    }

    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->reports);
    }
}
