<?php

namespace Hermes\Process;

use Hermes\Process\Health\Health;

class ProcessReport
{
    private string $process_id;
    private Health $health;
    private string $remarks;

    public function __construct(string $process_id, Health $health, string $remarks = '')
    {
        $this->process_id = $process_id;
        $this->health = $health;
        $this->remarks = $remarks;
    }

    public function isFor(string $candidate_id) : bool
    {
        return $candidate_id == $this->process_id;
    }

    public function health() : Health
    {
        return $this->health;
    }

    public function remarks() : string
    {
        return $this->remarks;
    }
}
