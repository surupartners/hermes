<?php

namespace Hermes\Process;

use Hermes\Process\Health\{
    AttemptingRestart,
    BackingOffForRestart,
    Dead,
    Running,
};
use Hermes\Process\Config\{
    ImmediateRestartLimit,
    ImmediateRestartLimitWindow,
    RestartBackOffLimit,
    RestartBackOffLimitWindow,
    RestartBackOffPeriod,
};
use Symfony\Component\Process\Process;

class Babysitter
{
    private \Closure $output_handler;
    /** @var BabysatProcess[] */
    private array $backed_off_processes = [];
    /** @var string[] */
    private array $dead_process_ids = [];
    /** @var BabysatProcess[] */
    private array $active_processes = [];

    public function __construct(callable $output_handler)
    {
        $this->output_handler = \Closure::fromCallable($output_handler);
    }

    public function watch(
        string $process_id,
        Process $process,
        ImmediateRestartLimit $immediate_restart_limit,
        ImmediateRestartLimitWindow $immediate_restart_limit_window,
        RestartBackOffPeriod $restart_back_off_period,
        RestartBackOffLimit $restart_back_off_limit,
        RestartBackOffLimitWindow $restart_back_off_limit_window,
    ) : void {
        $this->active_processes[$process_id] = new BabysatProcess(
            $process,
            $immediate_restart_limit,
            $immediate_restart_limit_window,
            $restart_back_off_period,
            $restart_back_off_limit,
            $restart_back_off_limit_window,
        );
    }

    public function checkOnProcesses() : ProcessesReport
    {
        $reports = [];

        foreach ($this->dead_process_ids as $id) {
            $reports[] = new ProcessReport($id, new Dead);
        }

        foreach ($this->backed_off_processes as $id => $process) {
            if ($process->lastBackedOffWithinBackOffWindow()) {
                $reports[] = new ProcessReport($id, new BackingOffForRestart);
            } else {
                $this->active_processes[$id] = $process;
                unset($this->backed_off_processes[$id]);
            }
        }

        foreach ($this->active_processes as $id => $process) {
            $process_health = new Running;

            if ($process->lastRestartedWithinSeconds(60)) {
                $process_health = new AttemptingRestart;
            }

            if ( ! $process->isRunning()) {
                $process_health = $process->attemptRestart($this->output_handler);

                if ($process_health instanceof BackingOffForRestart) {
                    $this->backed_off_processes[$id] = $process;
                    unset($this->active_processes[$id]);
                } else if ($process_health instanceof Dead) {
                    $this->dead_process_ids[] = $id;
                    unset($this->active_processes[$id]);
                }
            }

            $reports[] = new ProcessReport($id, $process_health);
        }

        return new ProcessesReport(...$reports);
    }
}
