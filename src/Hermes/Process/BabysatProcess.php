<?php

namespace Hermes\Process;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Hermes\Process\Config\ImmediateRestartLimit;
use Hermes\Process\Config\ImmediateRestartLimitWindow;
use Hermes\Process\Config\RestartBackOffLimit;
use Hermes\Process\Config\RestartBackOffLimitWindow;
use Hermes\Process\Config\RestartBackOffPeriod;
use Hermes\Process\Health\AttemptingRestart;
use Hermes\Process\Health\BackingOffForRestart;
use Hermes\Process\Health\Dead;
use Hermes\Process\Health\Health;
use Symfony\Component\Process\Process;
use function Functional\drop_first;
use function Functional\last;

class BabysatProcess
{
    /** @var CarbonInterface[] */
    private array $restarts = [];
    /** @var CarbonInterface[] */
    private array $back_offs = [];

    public function __construct(
        private Process $process,
        private ImmediateRestartLimit $immediate_restart_limit,
        private ImmediateRestartLimitWindow $immediate_restart_limit_window,
        private RestartBackOffPeriod $restart_back_off_period,
        private RestartBackOffLimit $restart_back_off_limit,
        private RestartBackOffLimitWindow $restart_back_off_limit_window,
    ) {}

    public function lastBackedOffWithinBackOffWindow() : bool
    {
        return
            ! empty($this->back_offs) &&
            Carbon::now()->lt(last($this->back_offs)->addSeconds($this->restart_back_off_period->toSeconds()));
    }

    public function lastRestartedWithinSeconds(int $time_in_seconds) : bool
    {
        return
            ! empty($this->restarts) &&
            Carbon::now()->lt(last($this->restarts)->addSeconds($time_in_seconds));
    }

    public function isRunning() : bool
    {
        return $this->process->isRunning();
    }

    public function attemptRestart(callable $output_handler) : Health
    {
        if ($this->mayBeImmediatelyRestarted()) {
            $this->process->start($output_handler);

            $this->restarts[] = CarbonImmutable::now();

            return new AttemptingRestart;
        }

        if ($this->mayBeBackedOff()) {
            $this->back_offs[] = CarbonImmutable::now();

            return new BackingOffForRestart;
        }

        return new Dead;
    }

    private function mayBeImmediatelyRestarted() : bool
    {
        $it_has_been_restarted_since_its_last_backoff =
            ! empty($this->restarts) &&
            ! empty($this->back_offs) &&
            last($this->restarts)->gte(last($this->back_offs));
        if ($it_has_been_restarted_since_its_last_backoff) {
            return false;
        }

        /** @var CarbonInterface[] $restarts_within_window */
        $restarts_within_window = drop_first($this->restarts, fn(CarbonInterface $restart) => $restart->lt(
            Carbon::now()->subSeconds($this->immediate_restart_limit_window->toSeconds()),
        ));

        // Forget restarts outside the window
        $this->restarts = $restarts_within_window;

        return $this->immediate_restart_limit->greaterThan(count($restarts_within_window));
    }

    private function mayBeBackedOff() : bool
    {
        $back_offs_within_window = drop_first($this->back_offs, fn(CarbonInterface $back_off) => $back_off->lt(
            Carbon::now()->subSeconds($this->restart_back_off_limit_window->toSeconds()),
        ));

        // Forget back offs outside the window
        $this->back_offs = $back_offs_within_window;

        return $this->restart_back_off_limit->greaterThan(count($back_offs_within_window));
    }
}
