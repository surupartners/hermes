<?php

namespace Hermes\Process\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class RestartBackOffLimitWindow implements TimePeriod
{
    use TimePeriodTrait;
}
