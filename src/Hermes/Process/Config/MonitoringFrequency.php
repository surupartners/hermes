<?php

namespace Hermes\Process\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class MonitoringFrequency implements TimePeriod
{
    use TimePeriodTrait;
}
