<?php

namespace Hermes\Process\Config;

class ImmediateRestartLimit
{
    private int $limit;

    public function __construct(int $immediate_restart_limit)
    {
        $this->limit = $immediate_restart_limit;
    }

    public function equals(self $comparator) : bool
    {
        return $this->limit == $comparator->limit;
    }

    public function greaterThan(int $comparator) : bool
    {
        return $this->limit > $comparator;
    }
}
