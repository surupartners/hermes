<?php

namespace Hermes\Process\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class RestartBackOffPeriod implements TimePeriod
{
    use TimePeriodTrait;
}
