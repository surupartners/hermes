<?php

namespace Hermes\Process\Config;

class RestartBackOffLimit
{
    private int $limit;

    public function __construct(int $restart_back_off_limit)
    {
        $this->limit = $restart_back_off_limit;
    }

    public function equals(self $comparator) : bool
    {
        return $this->limit == $comparator->limit;
    }

    public function greaterThan(int $comparator) : bool
    {
        return $this->limit > $comparator;
    }
}
