<?php

namespace Hermes\Process\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class ImmediateRestartLimitWindow implements TimePeriod
{
    use TimePeriodTrait;
}
