<?php

namespace Hermes\Process\Config;

use Illuminate\Config\Repository;

class Config
{
    private GracefulTerminationTimeout $graceful_termination_timeout;
    private MonitoringFrequency $monitoring_frequency;

    public function __construct(Repository $laravel_config)
    {
        $this->graceful_termination_timeout = GracefulTerminationTimeout::fromSeconds(
            $laravel_config->get('hermes.graceful_termination_timeout_in_seconds')
        );
        $this->monitoring_frequency = MonitoringFrequency::fromSeconds(
            $laravel_config->get('hermes.monitoring_frequency_in_seconds')
        );
    }

    public function gracefulTerminationTimout() : GracefulTerminationTimeout
    {
        return $this->graceful_termination_timeout;
    }

    public function monitoringFrequency() : MonitoringFrequency
    {
        return $this->monitoring_frequency;
    }
}
