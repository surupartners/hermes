<?php

namespace Hermes\Process\Config;

use Hermes\Common\{
    TimePeriod,
    TimePeriodTrait,
};

class GracefulTerminationTimeout implements TimePeriod
{
    use TimePeriodTrait;
}
