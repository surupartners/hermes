<?php

namespace spec\Hermes\StreamEvent;

use Hermes\Stream\StreamItem;
use stdClass;
use Hermes\StreamEvent\{
    Transformation,
    Transformer,
};
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TransformerSpec extends ObjectBehavior
{
    function let(Transformation $transformation, StreamItem $item, stdClass $event)
    {
        $transformation->matches($item)->willReturn(true);
        $transformation->matches(Argument::any())->willReturn(false);
        $transformation->transform($item)->willReturn($event);

        $this->registerTransformation($transformation);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Transformer::class);
    }

    function it_transforms_a_stream_item_to_the_matched_event(StreamItem $item, stdClass $event)
    {
        $this->transform($item)->shouldBe($event);
    }

    // Allows transformations to be overridden by later config
    function it_matches_a_stream_item_to_the_last_registered_matching_event(
        Transformation $other_transformation,
        StreamItem $item,
        stdClass $other_event,
    ) {
        $other_transformation->matches($item)->willReturn(true);
        $other_transformation->transform($item)->willReturn($other_event);

        $this->registerTransformation($other_transformation);

        $this->transform($item)->shouldBe($other_event);
    }

    function it_returns_null_if_there_is_no_registered_matching_event(StreamItem $other_item)
    {
        $this->transform($other_item)->shouldBeNull();
    }
}
