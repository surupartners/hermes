<?php

namespace spec\Hermes\StreamEvent;

use Hermes\Stream\StreamItem;
use Hermes\StreamEvent\{
    FailedToTransformStreamItem,
    InvalidTransformation,
    Transformation,
};
use PhpSpec\ObjectBehavior;
use stdClass;

class TransformationSpec extends ObjectBehavior
{
    function let(StreamItem $stream_item, stdClass $event)
    {
        $this->beConstructedWith(
            fn(StreamItem $candidate_item) => $candidate_item === $stream_item->getWrappedObject(),
            function(StreamItem $stream_item) use ($event) : stdClass {
                return $event->getWrappedObject();
            },
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Transformation::class);
    }

    function it_checks_whether_a_stream_item_matches_to_it(
        StreamItem $stream_item,
        StreamItem $other_stream_item
    ) {
        $this->matches($stream_item)->shouldBe(true);

        $this->matches($other_stream_item)->shouldBe(false);
    }

    function it_performs_its_transformation(StreamItem $stream_item, stdClass $event)
    {
        $this->transform($stream_item)->shouldBe($event);
    }

    function it_throws_an_exception_if_the_transformation_fails(StreamItem $item, stdClass $event)
    {
        $this->beConstructedWith(
            fn(StreamItem $stream_item) => true,
            function(StreamItem $stream_item) : stdClass {
                throw new \Exception('Test failure');
            },
        );

        $this->shouldThrow(FailedToTransformStreamItem::class)->duringTransform($item);
    }

    function it_rejects_transformations_which_do_not_declare_a_return_type(stdClass $event)
    {
        $this->beConstructedWith(
            fn(StreamItem $item) => true,
            function(StreamItem $item) use ($event) {
                return $event->getWrappedObject();
            },
        );

        $this->shouldThrow(InvalidTransformation::class)->duringInstantiation();
    }

    function it_rejects_transformations_which_declare_a_nullable_return_type(stdClass $event)
    {
        $this->beConstructedWith(
            fn(StreamItem $item) => true,
            function(StreamItem $item) use ($event) : ? stdClass {
                return $event;
            },
        );

        $this->shouldThrow(InvalidTransformation::class)->duringInstantiation();
    }

    function it_wraps_an_existing_transformation(StreamItem $stream_item, StreamItem $other_stream_item, stdClass $event)
    {
        $existing_transformation = new Transformation(
            fn(StreamItem $candidate_item) => $candidate_item === $stream_item,
            function(StreamItem $stream_item) use ($event) : stdClass {
                return $event->getWrappedObject();
            }
        );

        $this->beConstructedThrough('wrap', [
            $existing_transformation,
            function(StreamItem $candidate_item, callable $existing_matcher) use (
                $stream_item, $other_stream_item
            ) : bool {
                return $candidate_item === $other_stream_item->getWrappedObject();
            },
        ]);

        $this->matches($other_stream_item)->shouldBe(true);
        $this->matches($stream_item)->shouldBe(false);
    }
}
