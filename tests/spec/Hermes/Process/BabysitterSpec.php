<?php

namespace spec\Hermes\Process;

use Hermes\Process\{
    Babysitter,
    ProcessesReport,
    ProcessReport,
};
use Carbon\Carbon;
use Carbon\Carbonite;
use Hermes\Process\Config\{
    ImmediateRestartLimit,
    ImmediateRestartLimitWindow,
    RestartBackOffLimit,
    RestartBackOffLimitWindow,
    RestartBackOffPeriod,
};
use Hermes\Process\Health\{
    AttemptingRestart,
    BackingOffForRestart,
    Dead,
    Running,
};
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Process\Process;

class BabysitterSpec extends ObjectBehavior
{
    public function let(Process $process)
    {
        $this->beConstructedWith(function($type, $buffer) {
            // Do nothing
        });

        $process->isRunning()->willReturn(true);

        $this->watch(
            'test-id',
            $process,
            new ImmediateRestartLimit(2),
            ImmediateRestartLimitWindow::fromMinutes(5),
            RestartBackOffPeriod::fromHours(1),
            new RestartBackOffLimit(2),
            RestartBackOffLimitWindow::fromHours(24),
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Babysitter::class);
    }

    function it_does_not_restart_a_running_process(Process $process)
    {
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new Running),
        ));

        $process->start(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_checks_on_all_processes_it_is_watching(Process $process, Process $other_process)
    {
        $other_process->getPid()->willReturn(2);
        $other_process->isRunning()->willReturn(true);

        $this->watch(
            'other-id',
            $other_process,
            new ImmediateRestartLimit(2),
            ImmediateRestartLimitWindow::fromMinutes(5),
            RestartBackOffPeriod::fromHours(1),
            new RestartBackOffLimit(2),
            RestartBackOffLimitWindow::fromHours(24),
        );

        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new Running),
            new ProcessReport('other-id', new Running),
        ));
    }

    function it_restarts_a_failed_process_which_has_not_exceeded_the_immediate_restart_limit_within_the_immediate_restart_limit_window(
        Process $process,
    ) {
        $process->isRunning()->willReturn(false);

        $this->checkOnProcesses();
        $this->checkOnProcesses();

        Carbonite::jumpTo(Carbon::now()->addMinutes(5));
        $this->checkOnProcesses();
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new AttemptingRestart),
        ));

        $process->start(Argument::type('callable'))->shouldHaveBeenCalled();
    }

    function it_reports_a_restarted_process_as_running_if_it_is_still_running_a_minute_after_restart(
        Process $process,
    ) {
        $process->isRunning()->willReturn(false);
        $process->start(Argument::any())->willReturn();

        $this->checkOnProcesses();

        $process->isRunning()->willReturn(true);

        Carbonite::jumpTo(Carbon::now()->addSeconds(30));
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new AttemptingRestart),
        ));

        Carbonite::JumpTo(Carbon::now()->addSeconds(30));
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new Running()),
        ));
    }

    function it_backs_off_a_process_which_has_exceeded_the_immediate_restart_limit_within_the_immediate_restart_limit_window(
        Process $process,
    ) {
        $process->isRunning()->willReturn(false);

        $this->checkOnProcesses();
        $this->checkOnProcesses();
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new BackingOffForRestart),
        ));

        Carbonite::jumpTo(Carbon::now()->addHour());
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new AttemptingRestart),
        ));

        $process->start(Argument::type('callable'))->shouldHaveBeenCalled();
    }

    function it_gives_up_on_a_process_if_it_exceeds_the_restart_back_off_limit_within_the_restart_back_off_limit_window(
        Process $process,
    ) {
        $process->isRunning()->willReturn(false);
        $process->start(Argument::any())->willReturn();

        $this->checkOnProcesses();
        $this->checkOnProcesses();
        $this->checkOnProcesses();
        Carbonite::jumpTo(Carbon::now()->addHour());
        $this->checkOnProcesses();
        $this->checkOnProcesses();
        Carbonite::jumpTo(Carbon::now()->addHour());
        $this->checkOnProcesses();
        $this->checkOnProcesses()->shouldBeLike(new ProcessesReport(
            new ProcessReport('test-id', new Dead),
        ));
    }
}
