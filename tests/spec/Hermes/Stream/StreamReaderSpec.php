<?php

namespace spec\Hermes\Stream;

use Hermes\Event\ListenerId as ListenerId;
use Hermes\Stream\{
    StreamId,
    PositionTracker\PositionTracker,
    Stream,
    StreamItemId,
    StreamItem,
    StreamReader,
};
use PhpSpec\ObjectBehavior;

class StreamReaderSpec extends ObjectBehavior
{
    function let(
        Stream $stream,
        StreamId $stream_id,
        ListenerId $listener_id,
        PositionTracker $position_tracker,
        StreamItemId $last_item_id,
        StreamItem $new_stream_item,
        StreamItemId $new_item_id,
    ) {
        $stream->id()->willReturn($stream_id);
        $stream->itemFollowing($last_item_id)->willReturn($new_stream_item);

        $new_stream_item->id()->willReturn($new_item_id);

        $position_tracker->getLastItemFor($stream_id, $listener_id)->willReturn($last_item_id);

        $this->beConstructedWith($stream, $listener_id, $position_tracker);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamReader::class);
    }

    function it_returns_the_stream_item_following_the_last_item_it_returned(
        Stream $stream,
        StreamId $stream_id,
        ListenerId $listener_id,
        PositionTracker $position_tracker,
        StreamItem $new_stream_item,
        StreamItemId $new_item_id,
    ) {
        $this->nextItem()->shouldBe($new_stream_item);

        $position_tracker->updateLastItemFor($stream_id, $listener_id, $new_item_id)->shouldHaveBeenCalled();
    }

    function it_returns_null_if_there_is_no_such_stream_item(Stream $stream, StreamItemId $last_item_id)
    {
        $stream->itemFollowing($last_item_id)->willReturn(null);

        $this->nextItem()->shouldBeNull();
    }

    function it_returns_the_first_item_from_a_stream_it_has_not_read_before(
        Stream $stream,
        StreamId $stream_id,
        ListenerId $listener_id,
        PositionTracker $position_tracker,
        StreamItem $first_stream_item,
        StreamItemId $first_item_id,
    ) {
        $position_tracker->getLastItemFor($stream_id, $listener_id)->willReturn(null);

        $stream->firstItem()->willReturn($first_stream_item);

        $first_stream_item->id()->willReturn($first_item_id);

        $this->nextItem()->shouldBe($first_stream_item);

        $position_tracker->updateLastItemFor($stream_id, $listener_id, $first_item_id)->shouldHaveBeenCalled();
    }

    function it_returns_null_if_the_stream_does_not_have_a_first_item(
        Stream $stream,
        StreamId $stream_id,
        ListenerId $listener_id,
        PositionTracker $position_tracker,
    ) {
        $position_tracker->getLastItemFor($stream_id, $listener_id)->willReturn(null);

        $stream->firstItem()->willReturn(null);

        $this->nextItem()->shouldBeNull();
    }
}
