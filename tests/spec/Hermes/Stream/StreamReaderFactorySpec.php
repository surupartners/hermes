<?php

namespace spec\Hermes\Stream;

use Hermes\Event\ListenerId;
use Hermes\Stream\{
    PositionTracker\PositionTracker,
    Stream,
    StreamId,
    StreamReader,
    StreamReaderFactory,
    StreamRegistry,
};
use PhpSpec\ObjectBehavior;

class StreamReaderFactorySpec extends ObjectBehavior
{
    function let(
        PositionTracker $position_tracker,
        StreamRegistry $stream_registry,
        StreamId $stream_id,
        Stream $stream,
    ) {
        $stream_registry->find($stream_id)->willReturn($stream);

        $this->beConstructedWith($stream_registry, $position_tracker);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamReaderFactory::class);
    }

    function it_returns_a_stream_reader(
        StreamId $stream_id,
        ListenerId $listener_id,
        StreamRegistry $stream_registry,
    ) {
        $this->build($stream_id, $listener_id)->shouldBeAnInstanceOf(StreamReader::class);

        $stream_registry->find($stream_id)->shouldHaveBeenCalled();
    }
}
