<?php

namespace spec\Hermes\Stream;

use Hermes\Event\{
    ListenerId,
    ListenerRegistry,
};
use Hermes\Stream\{
    Config\StreamsConfig,
    StreamProcessorIdentifier,
    AvailableStreamProcessors,
    StreamId,
    StreamProcessorIdentifierSet,
};
use Illuminate\Log\Logger;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AvailableStreamProcessorsSpec extends ObjectBehavior
{
    function let(ListenerRegistry $listener_registry, StreamsConfig $streams_config, Logger $log)
    {
        $listener_registry->allIds()->willReturn([
            $first_listener_id = new ListenerId('A\Test\Listener'),
            $second_listener_id = new ListenerId('Other\Test\Listener'),
        ]);
        $listener_registry->streamsListenerIsInterestedIn($first_listener_id)->willReturn([
            $first_stream_id = new StreamId('First\Test\Stream'),
        ]);
        $listener_registry->streamsListenerIsInterestedIn($second_listener_id)->willReturn([
            $second_stream_id = new StreamId('Second\Test\Stream'),
            $third_stream_id = new StreamId('Third\Test\Stream'),
        ]);

        $streams_config->hasConfigFor($first_stream_id)->willReturn(true);
        $streams_config->hasConfigFor($second_stream_id)->willReturn(true);
        $streams_config->hasConfigFor($third_stream_id)->willReturn(true);

        $this->beConstructedWith($listener_registry, $streams_config, $log);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AvailableStreamProcessors::class);
    }

    function it_returns_a_set_of_identifiers_for_available_stream_processors()
    {
        $this->all()->shouldBeAnInstanceOf(StreamProcessorIdentifierSet::class);
    }

    function it_returns_a_stream_processor_identifier_for_each_combination_of_an_event_listener_and_a_stream_it_listens_for_events_from()
    {
        $this->all()->shouldIterateLike([
            new StreamProcessorIdentifier(new StreamId('First\Test\Stream'), new ListenerId('A\Test\Listener')),
            new StreamProcessorIdentifier(new StreamId('Second\Test\Stream'), new ListenerId('Other\Test\Listener')),
            new StreamProcessorIdentifier(new StreamId('Third\Test\Stream'), new ListenerId('Other\Test\Listener')),
        ]);
    }

    function it_warns_if_there_are_no_event_listeners(ListenerRegistry $listener_registry, Logger $log)
    {
        $listener_registry->allIds()->willReturn([]);

        $this->all()->shouldBeLike(new StreamProcessorIdentifierSet);

        $log->warning(Argument::any())->shouldHaveBeenCalled();
    }

    function it_warns_if_an_event_listener_does_not_listen_for_events_from_any_stream(
        ListenerRegistry $listener_registry,
        Logger $log,
    ) {
        $listener_registry->streamsListenerIsInterestedIn(new ListenerId('A\Test\Listener'))->willReturn([]);

        $this->all()->shouldBeAnInstanceOf(StreamProcessorIdentifierSet::class);

        $log->warning(Argument::any())->shouldHaveBeenCalled();
    }

    function it_throws_an_exception_if_an_event_listener_listens_for_events_from_an_unconfigured_stream(
        StreamsConfig $streams_config,
    ) {
        $streams_config->hasConfigFor(new StreamId('First\Test\Stream'))->willReturn(false);

        $this->shouldThrow(new \Exception(
            "Listener 'A\Test\Listener' configured to listen for events from an unconfigured stream."
        ))->duringInstantiation();
    }
}
