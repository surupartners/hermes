<?php

namespace spec\Hermes\Stream;

use Hermes\StreamEvent\Transformer;
use Illuminate\Log\Logger;
use Hermes\Event\{
    Listener,
    ListenerId,
    ListenerRegistry,
};
use Hermes\Tests\Support\Hermes\Transaction\DummyTransactionManager;
use Prophecy\Argument;
use stdClass;
use Hermes\Stream\{
    Config\RecheckDelay,
    StreamId,
    StreamItem,
    StreamItemId,
    StreamProcessor,
    StreamReader,
    StreamReaderFactory};
use PhpSpec\ObjectBehavior;

class StreamProcessorSpec extends ObjectBehavior
{
    function let(
        StreamReaderFactory $stream_reader_factory,
        StreamReader $stream_reader,
        StreamItem $stream_item,
        ListenerRegistry $listener_registry,
        Listener $listener,
        Transformer $transformer,
        stdClass $event,
        Logger $log,
        RecheckDelay $recheck_delay,
    ) {
        $stream_id = new StreamId('TestStream');
        $listener_id = new ListenerId('TestListener');
        $stream_item->id()->willReturn(new StreamItemId('Test Item'));
        $recheck_delay->toSeconds()->willReturn(0);

        $stream_reader_factory->build($stream_id, $listener_id)->willReturn($stream_reader);
        $listener_registry->find($listener_id)->willReturn($listener);

        $stream_reader->nextItem()->willReturn($stream_item);
        $transformer->transform($stream_item)->willReturn($event);

        $this->beConstructedWith(
            $stream_id,
            $listener_id,
            $recheck_delay,
            $stream_reader_factory,
            $listener_registry,
            $transformer,
            new DummyTransactionManager,
            $log,
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamProcessor::class);
    }

    function it_processes_a_stream_item_without_delay(
        Listener $listener,
        stdClass $event,
        RecheckDelay $recheck_delay,
    ) {
        $this->processNextItem();

        $listener->hear($event)->shouldHaveBeenCalled();
        $recheck_delay->toSeconds()->shouldNotHaveBeenCalled();
    }

    function it_waits_for_a_stream_item_if_none_are_available(
        StreamReader $stream_reader,
        Listener $listener,
        StreamItem $stream_item,
        stdClass $event,
        RecheckDelay $recheck_delay,
    ) {
        $stream_reader->nextItem()->willReturn(null, $stream_item);

        $this->processNextItem();

        $listener->hear($event)->shouldHaveBeenCalled();
        $recheck_delay->toSeconds()->shouldHaveBeenCalled();
    }

    function it_does_nothing_if_there_is_no_registered_transformation_for_the_item(
        Transformer $transformer,
        StreamItem $stream_item,
        Listener $listener,
    ) {
        $transformer->transform(Argument::any())->willReturn(null);

        $this->processNextItem();

        $listener->hear($stream_item)->shouldNotHaveBeenCalled();
    }
}
