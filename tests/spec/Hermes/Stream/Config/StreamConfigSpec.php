<?php

namespace spec\Hermes\Stream\Config;

use Hermes\Stream\{
    Config\ProcessingDelay,
    Config\RecheckDelay,
    Config\StreamConfig,
};
use Hermes\Tests\Support\Hermes\Event\{
    EventA,
    EventB,
};
use Illuminate\Config\Repository;
use PhpSpec\ObjectBehavior;

class StreamConfigSpec extends ObjectBehavior
{
    function let(Repository $laravel_config)
    {
        $laravel_config->has('hermes.recheck_delay_after_end_of_stream_in_seconds')->willReturn(true);
        $laravel_config->get('hermes.recheck_delay_after_end_of_stream_in_seconds')->willReturn(11);
        $laravel_config->has('hermes.processing_delay_between_stream_items_in_seconds')->willReturn(true);
        $laravel_config->get('hermes.processing_delay_between_stream_items_in_seconds')->willReturn(7);

        $this->beConstructedWith([
            'processing_delay_between_stream_items_in_seconds' => 9,
            'events' => [EventA::class],
        ], $laravel_config);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamConfig::class);
    }

    function it_returns_the_events_the_stream_provides()
    {
        $this->providesEvents()->shouldBeEqualTo([EventA::class]);
    }

    function it_checks_whether_it_provides_a_given_event()
    {
        $this->provides(EventA::class)->shouldBe(true);

        $this->provides(EventB::class)->shouldBe(false);
    }

    function it_throws_an_exception_if_the_stream_provides_no_events(Repository $laravel_config)
    {
        $this->beConstructedWith([
            'processing_delay_between_stream_items_in_seconds' => 9,
            'events' => [],
        ], $laravel_config);

        $this->shouldThrow(new \Exception(
            'All configured streams must have a non-empty list of provided events.'
        ))->duringInstantiation();
    }

    function it_returns_the_configured_recheck_delay()
    {
        $this->recheckDelay()->equals(RecheckDelay::fromSeconds(11))->shouldBe(true);
    }

    function it_returns_the_configured_processing_delay()
    {
        $this->processingDelay()->equals(ProcessingDelay::fromSeconds(9))->shouldBe(true);
    }

    function it_returns_a_raw_config_value()
    {
        $this->getConfigValue('processing_delay_between_stream_items_in_seconds')->shouldBe(9);
    }

    function it_checks_whether_it_has_a_raw_config_value(Repository $laravel_config)
    {
        $this->hasConfigValue('processing_delay_between_stream_items_in_seconds')->shouldBe(true);

        $laravel_config->has('hermes.nonsense_value')->willReturn(false);

        $this->hasConfigValue('nonsense_value')->shouldBe(false);
    }

    function it_returns_a_globally_configured_value_when_no_value_is_configured_for_the_stream(
        Repository $laravel_config
    ) {
        $this->beConstructedWith(['events' => [EventA::class]], $laravel_config);

        $this->processingDelay()->equals(ProcessingDelay::fromSeconds(7))->shouldBe(true);
    }
}
