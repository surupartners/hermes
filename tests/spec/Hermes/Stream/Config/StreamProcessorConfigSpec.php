<?php

namespace spec\Hermes\Stream\Config;

use Hermes\Event\ListenerId;
use Hermes\Stream\{
    Config\ProcessingDelay,
    Config\RecheckDelay,
    Config\StreamConfig,
    Config\StreamsConfig,
    StreamId,
    Config\StreamProcessorConfig};
use Hermes\Process\Config\{
    ImmediateRestartLimit,
    ImmediateRestartLimitWindow,
    RestartBackOffLimit,
    RestartBackOffLimitWindow,
    RestartBackOffPeriod,
};
use Illuminate\Config\Repository;
use PhpSpec\ObjectBehavior;

class StreamProcessorConfigSpec extends ObjectBehavior
{
    function let(Repository $laravel_config, StreamsConfig $streams_config, StreamConfig $stream_config)
    {
        $laravel_config->get('hermes.stream_processors')->willReturn([[
            'stream' => 'Example\Stream\Id',
            'listener' => 'Example\Listener\Id',
            'recheck_delay_after_end_of_stream_in_seconds' => 7,
            'processing_delay_between_stream_items_in_seconds' => 5,
            'immediate_restart_limit' => 3,
            'immediate_restart_limit_window_in_seconds' => 60,
            'restart_back_off_period_in_hours' => 1,
            'restart_back_off_limit' => 4,
            'restart_back_off_limit_window_in_hours' => 24,
        ]]);

        $streams_config->for($stream_id = new StreamId('Example\Stream\Id'))->willReturn($stream_config);

        $stream_config->hasConfigValue('processing_delay_between_stream_items_in_seconds')->willReturn(true);
        $stream_config->getConfigValue('processing_delay_between_stream_items_in_seconds')->willReturn(6);

        $this->beConstructedWith(
            $laravel_config,
            $streams_config,
            $stream_id,
            new ListenerId('Example\Listener\Id'),
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamProcessorConfig::class);
    }

    function it_returns_the_configured_recheck_delay()
    {
        $this->recheckDelay()->equals(RecheckDelay::fromSeconds(7))->shouldBe(true);
    }

    function it_returns_the_configured_processing_delay()
    {
        $this->processingDelay()->equals(ProcessingDelay::fromSeconds(5))->shouldBe(true);
    }

    function it_returns_the_configured_immediate_restart_limit()
    {
        $this->immediateRestartLimit()->equals(new ImmediateRestartLimit(3))->shouldBe(true);
    }

    function it_returns_the_configured_immediate_restart_limit_window()
    {
        $this->immediateRestartLimitWindow()->equals(ImmediateRestartLimitWindow::fromSeconds(60));
    }

    function it_returns_the_configured_restart_back_off_period()
    {
        $this->restartBackOffPeriod()->equals(RestartBackOffPeriod::fromHours(1));
    }

    function it_returns_the_configured_restart_back_off_limit()
    {
        $this->restartBackOffLimit()->equals(new RestartBackOffLimit(4))->shouldBe(true);
    }

    function it_returns_the_configured_restart_back_off_limit_window()
    {
        $this->restartBackOffLimitWindow()->equals(RestartBackOffLimitWindow::fromHours(24));
    }

    function it_returns_a_value_configured_for_a_stream_when_no_value_is_configured_for_the_processor(
        Repository $laravel_config
    ) {
        $laravel_config->get('hermes.stream_processors')->willReturn([]);

        $this->processingDelay()->equals(ProcessingDelay::fromSeconds(6))->shouldBe(true);
    }

    function it_throws_an_exception_if_the_value_is_unconfigured(
        Repository $laravel_config,
        StreamConfig $stream_config
    ) {
        $laravel_config->get('hermes.stream_processors')->willReturn([]);
        $stream_config->hasConfigValue('processing_delay_between_stream_items_in_seconds')->willReturn(false);

        $this->shouldThrow(\Exception::class)->duringProcessingDelay();
    }

    function it_throws_an_exception_if_a_stream_processors_config_lacks_a_stream(Repository $laravel_config)
    {
        $laravel_config->get('hermes.stream_processors')->willReturn([[
            'listener' => 'Example\Listener\Id',
        ]]);

        $this->shouldThrow(\Exception::class)->duringInstantiation();
    }

    function it_throws_an_exception_if_a_stream_processors_config_lacks_a_listener(Repository $laravel_config)
    {
        $laravel_config->get('hermes.stream_processors')->willReturn([[
            'stream' => 'Example\Stream\Id',
        ]]);

        $this->shouldThrow(\Exception::class)->duringInstantiation();
    }
}
