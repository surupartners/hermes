<?php

namespace spec\Hermes\Stream\Config;

use Hermes\Stream\{
    Config\StreamConfig,
    Config\StreamsConfig,
    StreamId,
    UnknownStream,
};
use Hermes\Tests\Support\Hermes\{
    Event\EventA,
    Event\EventB,
    Stream\StreamA,
    Stream\StreamB,
};
use Illuminate\Config\Repository;
use PhpSpec\ObjectBehavior;

class StreamsConfigSpec extends ObjectBehavior
{
    function let(Repository $laravel_config)
    {
        $laravel_config->get('hermes.streams')->willReturn([[
            'class' => StreamA::class,
            'events' => [EventA::class],
        ]]);

        $this->beConstructedWith($laravel_config);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamsConfig::class);
    }

    function it_fetches_a_streams_config_by_id(Repository $laravel_config)
    {
        $this->for(new StreamId(StreamA::class))->shouldBeLike(new StreamConfig([
            'class' => StreamA::class,
            'events' => [EventA::class],
        ], $laravel_config->getWrappedObject()));
    }

    function it_throws_an_exception_if_asked_for_config_for_an_unrecognised_stream()
    {
        $this->shouldThrow(UnknownStream::class)->during('for', [new StreamId(StreamB::class)]);
    }

    function it_throws_an_exception_if_a_streams_config_lacks_a_stream_class_value(Repository $laravel_config)
    {
        $laravel_config->get('hermes.streams')->willReturn([[
            'key' => 'value',
        ]]);

        $this->shouldThrow(new \Exception(
            'All configured streams must have a class value.'
        ))->duringInstantiation();
    }

    function it_checks_whether_it_has_config_for_a_stream()
    {
        $this->hasConfigFor(new StreamId(StreamA::class))->shouldBe(true);

        $this->hasConfigFor(new StreamId(StreamB::class))->shouldBe(false);
    }

    function it_determines_which_stream_a_given_event_originates_from(Repository $laravel_config)
    {
        $laravel_config->get('hermes.streams')->willReturn([[
            'class' => StreamA::class,
            'events' => [EventA::class],
        ], [
            'class' => StreamB::class,
            'events' => [EventB::class],
        ]]);

        $this->originatingStreamFor(EventA::class)->shouldBeLike(new StreamId(StreamA::class));
        $this->originatingStreamFor(EventB::class)->shouldBeLike(new StreamId(StreamB::class));
    }

    function it_throws_an_exception_if_given_an_unrecognised_event()
    {
        $this->shouldThrow(new \Exception(
            'No stream provides the event "' . EventB::class . '".'
        ))->during('originatingStreamFor', [EventB::class]);
    }

    function it_throws_an_exception_if_an_event_is_provided_by_more_than_one_stream(Repository $laravel_config)
    {
        $laravel_config->get('hermes.streams')->willReturn([[
            'class' => StreamA::class,
            'events' => [EventA::class],
        ], [
            'class' => StreamB::class,
            'events' => [EventA::class],
        ]]);

        $this->shouldThrow(new \Exception(
            'The events "' . EventA::class . '" provided by "' . StreamB::class .
            '" are already provided by other streams.'
        ))->duringInstantiation();
    }

    function it_throws_an_exception_if_streams_have_the_same_class_value(Repository $laravel_config)
    {
        $laravel_config->get('hermes.streams')->willReturn([[
            'class' => StreamA::class,
            'events' => [EventA::class],
        ], [
            'class' => StreamA::class,
            'events' => [EventB::class],
        ]]);

        $this->shouldThrow(new \Exception(
            "The stream '" . StreamA::class . "' is configured more than once."
        ))->duringInstantiation();
    }
}
