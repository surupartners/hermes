<?php

namespace spec\Hermes\Stream;

use Hermes\Event\ListenerId;
use Hermes\Stream\{
    StreamId,
    StreamProcessorIdentifier,
    StreamProcessorIdentifierSet,
};
use PhpSpec\ObjectBehavior;

class StreamProcessorIdentifierSetSpec extends ObjectBehavior
{
    private array $identifiers = [];

    function let()
    {
        $this->beConstructedWith(...$this->identifiers());
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(StreamProcessorIdentifierSet::class);
    }

    function it_iterates_over_the_stream_processor_identifiers_it_is_constructed_with()
    {
        $this->shouldIterateAs($this->identifiers());
    }

    function it_calculates_a_new_set_of_its_contents_excluding_a_given_set()
    {
        $this->excluding(new StreamProcessorIdentifierSet(
            $this->identifiers()[2],
            $this->identifiers()[3],
            $this->identifiers()[7],
        ))->shouldIterateAs([
            $this->identifiers()[0],
            $this->identifiers()[1],
            $this->identifiers()[4],
            $this->identifiers()[5],
            $this->identifiers()[6],
            $this->identifiers()[8],
        ]);
    }

    function it_can_exclude_a_large_set_from_a_small_one()
    {
        $this->beConstructedWith($this->identifiers[2]);

        $this->excluding(new StreamProcessorIdentifierSet(...$this->identifiers))->shouldIterateAs([]);
    }

    function it_calculates_the_intersection_of_itself_and_a_given_set()
    {
        $this->intersecting(new StreamProcessorIdentifierSet(
            $this->identifiers()[0],
            $this->identifiers()[2],
            $this->identifiers()[3],
            new StreamProcessorIdentifier(new StreamId('Test Stream 5'), new ListenerId('Test Listener 10')),
            new StreamProcessorIdentifier(new StreamId('Test Stream 5'), new ListenerId('Test Listener 11')),
        ))->shouldIterateAs([
            $this->identifiers()[0],
            $this->identifiers()[2],
            $this->identifiers()[3],
        ]);
    }

    function it_is_countable()
    {
        $this->shouldImplement(\Countable::class);
    }

    private function identifiers() : array
    {
        if (empty($this->identifiers)) $this->identifiers = [
            new StreamProcessorIdentifier(
                new StreamId('Core\Hermes\PositionsStream'),
                new ListenerId('Applications\DDD\Infrastructure\Integration\RequisitionsEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Core\Hermes\RequisitionsStream'),
                new ListenerId('Applications\DDD\Infrastructure\Integration\RequisitionsEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Core\Hermes\PersonProfilesStream'),
                new ListenerId('PersonProfiles\DDD\Infrastructure\PersonProfile\CharisIdToPersonProfileIdMap'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Requisitions\DDD\Infrastructure\Streams\RequisitionsStream'),
                new ListenerId('Requisitions\DDD\Infrastructure\Streams\RequisitionsListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Workflows\DDD\Infrastructure\Streams\ApplicationsStream'),
                new ListenerId('Workflows\DDD\Infrastructure\Streams\ApplicationsEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Workflows\DDD\Infrastructure\Streams\PersonProfileStream'),
                new ListenerId('Workflows\DDD\Infrastructure\Streams\PersonProfileEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Workflows\DDD\Infrastructure\Streams\PositionsStream'),
                new ListenerId('Workflows\DDD\Infrastructure\Streams\PositionsEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Workflows\DDD\Infrastructure\Streams\WorkflowsStream'),
                new ListenerId('Workflows\DDD\Infrastructure\Streams\WorkflowsEventListener'),
            ),
            new StreamProcessorIdentifier(
                new StreamId('Workflows\DDD\Infrastructure\Streams\RequisitionsStream'),
                new ListenerId('Workflows\DDD\Infrastructure\Streams\RequisitionsEventListener'),
            ),
        ];

        return $this->identifiers;
    }
}
