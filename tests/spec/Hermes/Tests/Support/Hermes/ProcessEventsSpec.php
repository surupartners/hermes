<?php

namespace spec\Hermes\Tests\Support\Hermes;

use Hermes\Event\{
    Listener,
    ListenerId,
    ListenerRegistry,
};
use Hermes\Stream\{
    AvailableStreamProcessors,
    Stream,
    StreamId,
    StreamItem,
    StreamItemId,
    StreamProcessorIdentifier,
    StreamProcessorIdentifierSet,
    StreamRegistry,
};
use Hermes\StreamEvent\Transformer;
use Hermes\Tests\Support\Hermes\ProcessEvents;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use stdClass;

class ProcessEventsSpec extends ObjectBehavior
{
    function let(
        AvailableStreamProcessors $available_stream_processors,
        StreamRegistry $stream_registry,
        Stream $stream_a,
        Stream $stream_b,
        ListenerRegistry $listener_registry,
        Listener $listener_a1,
        Listener $listener_b1,
        Listener $listener_a2,
        Listener $listener_b2,
        Listener $listener_a3,
        Listener $listener_b3,
        Transformer $transformer,
        StreamItem $stream_a_item,
        StreamItem $stream_b_item,
        stdClass $stream_a_event,
        stdClass $stream_b_event,
    ) {
        $available_stream_processors->all()->willReturn(new StreamProcessorIdentifierSet(
            new StreamProcessorIdentifier(new StreamId('Stream A'), new ListenerId('Listener A1')),
            new StreamProcessorIdentifier(new StreamId('Stream B'), new ListenerId('Listener B1')),
            new StreamProcessorIdentifier(new StreamId('Stream A'), new ListenerId('Listener A2')),
            new StreamProcessorIdentifier(new StreamId('Stream B'), new ListenerId('Listener B2')),
            new StreamProcessorIdentifier(new StreamId('Stream A'), new ListenerId('Listener A3')),
            new StreamProcessorIdentifier(new StreamId('Stream B'), new ListenerId('Listener B3')),
        ));

        $stream_a->id()->willReturn(new StreamId('Stream A'));
        $stream_b->id()->willReturn(new StreamId('Stream B'));
        $stream_registry->find(new StreamId('Stream A'))->willReturn($stream_a);
        $stream_registry->find(new StreamId('Stream B'))->willReturn($stream_b);

        $listener_registry->find(new ListenerId('Listener A1'))->willReturn($listener_a1);
        $listener_registry->find(new ListenerId('Listener B1'))->willReturn($listener_b1);
        $listener_registry->find(new ListenerId('Listener A2'))->willReturn($listener_a2);
        $listener_registry->find(new ListenerId('Listener B2'))->willReturn($listener_b2);
        $listener_registry->find(new ListenerId('Listener A3'))->willReturn($listener_a3);
        $listener_registry->find(new ListenerId('Listener B3'))->willReturn($listener_b3);

        $stream_a->firstItem()->willReturn($stream_a_item);
        $stream_a->itemFollowing(Argument::any())->willReturn(null);
        $stream_a_item->id()->willReturn(new StreamItemId('Stream A Item'));

        $stream_b->firstItem()->willReturn($stream_b_item);
        $stream_b->itemFollowing(Argument::any())->willReturn(null);
        $stream_b_item->id()->willReturn(new StreamItemId('Stream B Item'));

        $transformer->transform($stream_a_item)->willReturn($stream_a_event);
        $transformer->transform($stream_b_item)->willReturn($stream_b_event);

        $this->beConstructedWith(
            $available_stream_processors,
            $listener_registry,
            $transformer,
            $stream_registry,
        );
    }

    function it_is_initializable()
    {
        $this->shouldBeAnInstanceOf(ProcessEvents::class);
    }

    function it_processes_events_with_listeners_interested_in_specified_stream(
        Listener $listener_a1,
        Listener $listener_b1,
        Listener $listener_a2,
        Listener $listener_b2,
        Listener $listener_a3,
        Listener $listener_b3,
        stdClass $stream_a_event,
        stdClass $stream_b_event,
    ) {
        $this->onStream(new StreamId('Stream A'));

        $listener_a1->hear($stream_a_event)->shouldHaveBeenCalled();
        $listener_a2->hear($stream_a_event)->shouldHaveBeenCalled();
        $listener_a3->hear($stream_a_event)->shouldHaveBeenCalled();

        $listener_b1->hear($stream_b_event)->shouldNotHaveBeenCalled();
        $listener_b2->hear($stream_b_event)->shouldNotHaveBeenCalled();
        $listener_b3->hear($stream_b_event)->shouldNotHaveBeenCalled();
    }
}
