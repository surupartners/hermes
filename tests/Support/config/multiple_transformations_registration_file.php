<?php

use Hermes\Stream\StreamItem;
use Hermes\Tests\Support\Hermes\Event\{
    EventA,
    EventB,
};

return [[
    'matching_predicate' => function(StreamItem $item) : bool {
        return true;
    },
    'transformation' => function(StreamItem $item) : EventA {
        return new EventA;
    },
], [
    'matching_predicate' => function(StreamItem $item) : bool {
        return true;
    },
    'transformation' => function(StreamItem $item) : EventB {
        return new EventB;
    }
]];
