<?php

use Hermes\Stream\StreamItem;
use Hermes\Tests\Support\Hermes\Event\EventC;

return [[
    'matching_predicate' => function(StreamItem $item) : bool {
        return true;
    },
    'transformation' => function(StreamItem $item) : EventC {
        return new EventC;
    },
]];
