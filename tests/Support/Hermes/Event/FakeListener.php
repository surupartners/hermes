<?php

namespace Hermes\Tests\Support\Hermes\Event;

use Hermes\Event\Listener;
use Hermes\Event\ListenerId;

class FakeListener implements Listener
{
    public function id() : ListenerId
    {
        return new ListenerId(self::class);
    }

    public function hear($event) : void
    {
    }
}
