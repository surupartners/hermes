<?php

namespace Hermes\Tests\Support\Hermes;

use Hermes\Event\{
    Listener,
    ListenerRegistry,
};
use Hermes\Stream\{
    AvailableStreamProcessors,
    Stream,
    StreamId,
    StreamItemId,
    StreamProcessorIdentifier,
    StreamRegistry,
};
use Hermes\StreamEvent\Transformer;
use function Functional\each;

class ProcessEvents
{
    private ? StreamItemId $previous_item = null;

    public function __construct(
        private AvailableStreamProcessors $processors,
        private ListenerRegistry $listeners,
        private Transformer $transformer,
        private StreamRegistry $streams,
    ) {}

    public function onStream(Stream|StreamId|string $stream) : void
    {
        $stream = $this->findStream($stream);
        $listeners = $this->listenersInterestedInStream($stream->id());

        $item = is_null($this->previous_item)
            ? $stream->firstItem()
            : $stream->itemFollowing($this->previous_item);

        while ($item) {
            $event = $this->transformer->transform($item);

            each($listeners, fn (Listener $listener) => $listener->hear($event));

            $this->previous_item = $item->id();
            $item = $stream->itemFollowing($item->id());
        }
    }

    private function findStream(Stream|StreamId|string $stream) : Stream
    {
        if ($stream instanceof Stream) {
            return $stream;
        }
        if ($stream instanceof StreamId) {
            return $this->streams->find($stream);
        }
        return $this->streams->find(new StreamId($stream));
    }

    /** @return Listener[] */
    private function listenersInterestedInStream(StreamId $stream) : array
    {
        $listeners = [];
        /** @var StreamProcessorIdentifier $processor */
        foreach ($this->processors->all() as $processor) {
            if ($processor->streamId()->equals($stream)) {
                $listeners[] = $this->listeners->find($processor->listenerId());
            }
        }
        return $listeners;
    }
}
