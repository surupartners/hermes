<?php

namespace Hermes\Tests\Support\Hermes\Transaction;

use Hermes\Transaction\TransactionManager;

class DummyTransactionManager implements TransactionManager
{
    function transaction(callable $transaction) : void
    {
        $transaction();
    }
}
