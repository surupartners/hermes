<?php

namespace Hermes\Tests\Support\Hermes\Stream;

use Hermes\Stream\{
    Stream,
    StreamId,
    StreamItem,
    StreamItemId,
};

class StubStream implements Stream
{
    function id() : StreamId
    {
        return new StreamId(self::class);
    }

    function itemFollowing(StreamItemId $item_id) : ? StreamItem
    {
        return null;
    }

    public function firstItem() : ? StreamItem
    {
        return new StreamItem(new StreamItemId('1'), $this->id(), '');
    }
}
