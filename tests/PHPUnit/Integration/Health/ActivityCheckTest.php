<?php

namespace Hermes\Tests\PHPUnit\Integration\Health;

use Hermes\Monitoring\Health\ActivityCheck;
use Hermes\Tests\PHPUnit\Integration\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use function Functional\map;

class ActivityCheckTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_succeeds_if_there_are_no_processor_state_records()
    {
        $result = (new ActivityCheck())->run();

        $this->assertSame('ok', $result->status->label);
        $this->assertSame('ok', $result->status->value);
    }

    /** @test */
    function it_fails_if_a_processor_has_not_updated_for_24_hours()
    {
        $this->hermesStreamProcessorStatesFactory([
            [],
            ['updated' => $failTime1 = now()->subHours(24)],
            [],
            ['updated' => $failTime2 = now()->subHours(30)],
        ]);

        $result = (new ActivityCheck())->run();

        $this->assertSame('failed', $result->status->label);
        $this->assertSame('failed', $result->status->value);
        $this->assertSame(map([[
            'stream_id' => 'stream2',
            'listener_id' => 'listener2',
            'state' => 'Running',
            'remarks' => '',
            'updated_at' => $failTime1->toDateTimeString(),
        ], [
            'stream_id' => 'stream4',
            'listener_id' => 'listener4',
            'state' => 'Running',
            'remarks' => '',
            'updated_at' => $failTime2->toDateTimeString(),
        ]], fn(array $item) => json_encode($item)), $result->meta);
    }

    /** @test */
    function it_succeeds_if_all_processors_have_updated_within_24_hours()
    {
        $this->hermesStreamProcessorStatesFactory([
            ['updated' => now()],
            ['updated' => now()->subHours(1)],
            ['updated' => now()->subHours(23)],
        ]);

        $result = (new ActivityCheck())->run();

        $this->assertSame('ok', $result->status->label);
        $this->assertSame('ok', $result->status->value);
    }

    //
    // HELPERS
    //

    private function hermesStreamProcessorStatesFactory(array $states)
    {
        $statesTable = config('hermes.stream_processor_states_table_name');

        $id = 1;
        foreach($states as $state) {
            DB::table($statesTable)->insert([
                'stream_id' => 'stream' . $id,
                'listener_id' => 'listener' . $id,
                'state' => $state['state'] ?? 'Running',
                'remarks' => $state['remark'] ?? '',
                'updated_at' => $state['updated'] ?? now(),
            ]);
            ++$id;
        }
    }
}
