<?php

namespace Hermes\Tests\PHPUnit\Integration\Health;

use Hermes\Monitoring\Health\ConfigurationCheck;
use Hermes\Stream\AvailableStreamProcessors;
use Hermes\Tests\PHPUnit\Integration\TestCase;

class ConfigurationCheckTest extends TestCase
{
    function setUp() : void
    {
        parent::setUp();

        $available_stream_processors = $this->mock(AvailableStreamProcessors::class);
        $available_stream_processors->shouldReceive('isEmpty')->andReturn(false);
    }

    /** @test */
    function it_returns_ok()
    {
        $result = (new ConfigurationCheck())->run();

        $this->assertSame('ok', $result->status->label);
        $this->assertSame('ok', $result->status->value);
    }

    /** @test */
    function it_warns_if_there_are_no_configured_stream_processors()
    {
        $available_stream_processors = $this->mock(AvailableStreamProcessors::class);
        $available_stream_processors->shouldReceive('isEmpty')->andReturn(true);

        $result = (new ConfigurationCheck())->run();

        $this->assertSame('warning', $result->status->label);
        $this->assertSame('warning', $result->status->value);
    }
}
