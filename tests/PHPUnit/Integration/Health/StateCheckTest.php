<?php

namespace Hermes\Tests\PHPUnit\Integration\Health;

use Hermes\Monitoring\Health\StateCheck;
use Hermes\Tests\PHPUnit\Integration\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use function Functional\map;

class StateCheckTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_returns_ok()
    {
        $result = (new StateCheck())->run();

        $this->assertSame('ok', $result->status->label);
        $this->assertSame('ok', $result->status->value);
    }

    /** @test */
    function it_fails_if_any_processors_are_dead()
    {
        $this->hermesStreamProcessorStatesFactory([
            ['state' => 'Running'],
            ['state' => 'Dead'],
            ['state' => 'Running'],
            ['state' => 'Dead'],
            ['state' => 'Running'],
        ]);

        $result = (new StateCheck())->run();

        $this->assertSame('failed', $result->status->label);
        $this->assertSame('failed', $result->status->value);
        $this->assertSame(
            'There are dead processors (2)',
            $result->notificationMessage
        );
    }

    /** @test */
    function it_warns_if_any_processors_are_not_running_and_not_dead()
    {
        $this->hermesStreamProcessorStatesFactory([
            ['state' => 'Attempting'],
            ['state' => 'Running'],
            ['state' => 'Restarting'],
            ['state' => 'Running'],
            ['state' => 'Attempting'],
        ]);

        $result = (new StateCheck())->run();

        $this->assertSame('warning', $result->status->label);
        $this->assertSame('warning', $result->status->value);
        $this->assertSame(
            'There are processors not fully running (3)',
            $result->notificationMessage
        );
    }

    /** @test */
    function it_adds_the_processor_remarks_to_a_failure()
    {
        $this->hermesStreamProcessorStatesFactory([
            ['state' => 'Dead', 'remark' => 'Exit code 5'],
            ['state' => 'Dead', 'remark' => 'Exit code 15'],
        ]);

        $result = (new StateCheck())->run();

        $this->assertSame(map([[
            'stream_id' => 'stream1',
            'listener_id' => 'listener1',
            'state' => 'Dead',
            'remark' => 'Exit code 5'
        ],
        [
            'stream_id' => 'stream2',
            'listener_id' => 'listener2',
            'state' => 'Dead',
            'remark' => 'Exit code 15'
        ]], fn(array $item) => json_encode($item)), $result->meta);
    }

    /** @test */
    function it_adds_the_processor_remarks_to_a_warning()
    {
        $this->hermesStreamProcessorStatesFactory([
            ['state' => 'Restarting', 'remark' => 'Exit code 5'],
            ['state' => 'Restarting', 'remark' => 'Exit code 15'],
        ]);

        $result = (new StateCheck())->run();

        $this->assertSame(map([[
            'stream_id' => 'stream1',
            'listener_id' => 'listener1',
            'state' => 'Restarting',
            'remark' => 'Exit code 5'
        ], [
            'stream_id' => 'stream2',
            'listener_id' => 'listener2',
            'state' => 'Restarting',
            'remark' => 'Exit code 15'
        ]], fn(array $item) => json_encode($item)), $result->meta);
    }

    //
    // HELPERS
    //

    private function hermesStreamProcessorStatesFactory(array $states)
    {
        $id = 1;
        foreach($states as $state) {
            DB::table('hermes_stream_processor_states')->insert([
                'stream_id' => 'stream' . $id,
                'listener_id' => 'listener' . $id,
                'state' => $state['state'],
                'remarks' => $state['remark'] ?? '',
            ]);
            ++$id;
        }
    }

}
