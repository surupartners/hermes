<?php

namespace Hermes\Tests\PHPUnit\Integration;

use Hermes\Stream\{
    StreamId,
    StreamItem,
    StreamItemId,
};
use Hermes\StreamEvent\{
    TransformationConfig,
    TransformationList,
    Transformer,
};
use Hermes\Tests\Support\Hermes\Event\{
    EventA,
    EventB,
    EventC,
};
use Hermes\Tests\Support\Hermes\Stream\StubStream;
use Illuminate\Config\Repository;
use Illuminate\Log\Logger;

class TransformationConfigTest extends TestCase
{
    private TransformationConfig $transformation_config;
    private Transformer $transformer;

    private Repository $mock_laravel_config;
    private Logger $log_spy;

    function setUp() : void
    {
        parent::setUp();

        $this->transformer = new Transformer;

        $this->mock_laravel_config = \Mockery::mock(Repository::class);
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.streams')
            ->andReturn([[
                'class' => StubStream::class,
                'events' => [
                    EventA::class,
                    EventB::class,
                    EventC::class,
                ],
            ]]);

        $this->log_spy = \Mockery::spy(Logger::class);
    }

    /** @test */
    function it_returns_a_list_of_transformations_to_be_registered()
    {
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn(__DIR__ . '/../../Support/config/single_transformation_registration_file.php');
        $this->transformation_config = new TransformationConfig($this->mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();

        $this->assertInstanceOf(TransformationList::class, $transformations);
    }

    /** @test */
    function it_works_with_an_array_of_transformation_file_paths_as_well_as_a_single_path()
    {
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn([
                __DIR__ . '/../../Support/config/single_transformation_registration_file.php',
                __DIR__ . '/../../Support/config/multiple_transformations_registration_file.php',
            ]);
        $this->transformation_config = new TransformationConfig($this->mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();

        $this->assertInstanceOf(TransformationList::class, $transformations);
    }

    // The transformer gives precedence to later registered transformations over earlier ones. It does this in order to
    // allow config to be overridden where needed. Accordingly, later transformation registration files take precedence
    // over earlier ones. However, within a given registration file, we want a earlier registrations to take precedence
    // over later ones since that's the most natural way to read a config file.
    /** @test */
    function it_registers_earlier_transformations_in_a_file_after_later_ones()
    {
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn(__DIR__ . '/../../Support/config/multiple_transformations_registration_file.php');
        $this->transformation_config = new TransformationConfig($this->mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();

        foreach ($transformations as $transformation) {
            $this->transformer->registerTransformation($transformation);
        }

        $event = $this->transformer->transform(new StreamItem(
            new StreamItemId(1), new StreamId(StubStream::class), '',
        ));

        $this->assertInstanceOf(EventA::class, $event);
    }

    /** @test */
    function it_registers_the_transformations_in_earlier_files_before_those_in_later_ones()
    {
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn([
                __DIR__ . '/../../Support/config/multiple_transformations_registration_file.php',
                __DIR__ . '/../../Support/config/single_transformation_registration_file.php',
            ]);
        $this->transformation_config = new TransformationConfig($this->mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();

        foreach ($transformations as $transformation) {
            $this->transformer->registerTransformation($transformation);
        }

        $event = $this->transformer->transform(new StreamItem(
            new StreamItemId(1), new StreamId(StubStream::class), '',
        ));

        $this->assertInstanceOf(EventC::class, $event);
    }

    /** @test */
    function it_warns_if_the_transformation_list_is_empty()
    {
        $this->mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn(__DIR__ . '/../../Support/config/empty_transformations_file.php');
        $this->transformation_config = new TransformationConfig($this->mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();

        $this->assertInstanceOf(TransformationList::class, $transformations);
        $this->log_spy->shouldHaveReceived('warning')->withAnyArgs();
    }

    /** @test */
    function it_ensures_events_are_only_returned_from_their_configured_stream()
    {
        $mock_laravel_config = \Mockery::mock(Repository::class);
        $mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.stream_item_transformations_registration_path')
            ->andReturn(__DIR__ . '/../../Support/config/multiple_transformations_registration_file.php');
        $mock_laravel_config
            ->shouldReceive('get')
            ->with('hermes.streams')
            ->andReturn([[
                'class' => StubStream::class,
                'events' => [EventB::class],
            ]]);

        $this->transformation_config = new TransformationConfig($mock_laravel_config, $this->log_spy);

        $transformations = $this->transformation_config->configuredTransformations();
        foreach ($transformations as $transformation) {
            $this->transformer->registerTransformation($transformation);
        }

        $event = $this->transformer->transform(new StreamItem(
            new StreamItemId('1'),
            new StreamId(StubStream::class),
            ''
        ));

        $this->assertInstanceOf(EventB::class, $event);
        $this->log_spy->shouldHaveReceived('warning')->withAnyArgs();
    }
}
