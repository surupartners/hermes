<?php

namespace Hermes\Tests\PHPUnit\Integration;

use Hermes\Event\ListenerId;
use Hermes\Stream\PositionTracker\PositionTracker;
use Hermes\Stream\StreamId;
use Hermes\Stream\StreamItemId;
use Illuminate\Support\Facades\App;

class PositionTrackerTest extends TestCase
{
    private PositionTracker $position_tracker;

    function setUp() : void
    {
        parent::setUp();

        $this->position_tracker = App::make(PositionTracker::class);
    }

    /** @test */
    function it_stores_the_id_of_the_last_item_read_from_a_stream_for_a_given_listener()
    {
        $stream_id = new StreamId('test id');
        $listener_id = new ListenerId('test id');
        $item_id = new StreamItemId('test id');

        $this->position_tracker->updateLastItemFor($stream_id, $listener_id, $item_id);

        $stored_item_id = $this->position_tracker->getLastItemFor($stream_id, $listener_id);

        $this->assertTrue($item_id->equals($stored_item_id));
    }

    /** @test */
    function it_returns_null_if_no_item_id_has_been_set_for_a_given_stream_and_listener()
    {
        $stream_id = new StreamId('test id');
        $listener_id = new ListenerId('test id');

        $item_id = $this->position_tracker->getLastItemFor($stream_id, $listener_id);

        $this->assertNull($item_id);
    }
}
