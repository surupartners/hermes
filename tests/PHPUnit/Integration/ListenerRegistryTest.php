<?php

namespace Hermes\Tests\PHPUnit\Integration;

use Illuminate\Support\Facades\App;
use Hermes\Stream\{
    StreamId,
    Config\StreamsConfig,
};
use Hermes\Tests\Support\Hermes\Stream\{
    StreamA,
    StreamB,
};
use Hermes\Event\{
    ListenerAlreadyRegistered,
    ListenerId,
    ListenerRegistry,
};
use Hermes\Tests\Support\Hermes\Event\{
    EventA,
    EventB,
    EventC,
    FakeListener,
};
use Hermes\Event\UnknownListener;

class ListenerRegistryTest extends TestCase
{
    private ListenerRegistry $listener_registry;
    private StreamsConfig $mock_streams_config;

    function setUp() : void
    {
        parent::setUp();

        $this->mock_streams_config = \Mockery::mock(StreamsConfig::class);
        $this->mock_streams_config
            ->shouldReceive('originatingStreamFor')
            ->with(EventA::class)
            ->andReturn(new StreamId(StreamA::class));
        $this->mock_streams_config
            ->shouldReceive('originatingStreamFor')
            ->with(EventB::class)
            ->andReturn(new StreamId(StreamB::class));
        $this->mock_streams_config
            ->shouldReceive('originatingStreamFor')
            ->with(EventC::class)
            ->andReturn(new StreamId(StreamA::class));

        $this->listener_registry = new ListenerRegistry($this->mock_streams_config, App::getFacadeApplication());
    }

    /** @test */
    function it_finds_a_listener_by_its_id()
    {
        $id = new ListenerId(FakeListener::class);

        $this->listener_registry->register($id, fn() => new FakeListener);

        $returned_listener = $this->listener_registry->find($id);

        $this->assertInstanceOf(FakeListener::class, $returned_listener);
    }

    /** @test */
    function it_fetches_the_streams_from_which_an_event_listener_listens_for_events()
    {
        $id = new ListenerId(FakeListener::class);
        $stream_a_id = new StreamId(StreamA::class);
        $stream_b_id = new StreamId(StreamB::class);

        $this->listener_registry->register($id, fn() => new FakeListener, $stream_a_id, $stream_b_id);

        $returned_streams = $this->listener_registry->streamsListenerIsInterestedIn($id);

        $this->assertCount(2, $returned_streams);
        $this->assertContainsEquals($stream_a_id, $returned_streams);
        $this->assertContainsEquals($stream_b_id, $returned_streams);
    }

    /** @test */
    function it_throws_an_exception_if_asked_for_streams_for_an_unregistered_listener()
    {
        $this->expectException(UnknownListener::class);

        $this->listener_registry->streamsListenerIsInterestedIn(new ListenerId(FakeListener::class));
    }

    /** @test */
    function it_registers_a_listener_and_the_events_it_listens_for()
    {
        $listener = new FakeListener;
        $listener_id = new ListenerId($listener::class);

        $this->listener_registry->register(
            $listener,
            fn() => new FakeListener,
            EventA::class,
            EventB::class,
            EventC::class
        );

        $returned_listener = $this->listener_registry->find($listener_id);
        $returned_streams = $this->listener_registry->streamsListenerIsInterestedIn($listener_id);

        $this->assertEquals($listener, $returned_listener);
        $this->assertCount(2, $returned_streams);
        $this->assertContainsEquals(new StreamId(StreamA::class), $returned_streams);
        $this->assertContainsEquals(new StreamId(StreamB::class), $returned_streams);
    }

    /** @test */
    function it_throws_an_exception_if_asked_for_an_unregistered_listener()
    {
        $this->expectException(UnknownListener::class);

        $this->listener_registry->find(new ListenerId(FakeListener::class));
    }

    /** @test */
    function it_throws_an_exception_if_asked_for_the_streams_an_unregistered_listener_is_interested_in()
    {
        $this->expectException(UnknownListener::class);

        $this->listener_registry->streamsListenerIsInterestedIn(new ListenerId(FakeListener::class));
    }

    /** @test */
    function it_throws_an_exception_when_attempting_to_register_an_already_registered_listener()
    {
        $listener = new FakeListener;
        $listener_id = new ListenerId($listener::class);

        $this->listener_registry->register($listener, fn() => new FakeListener);

        try {
            $this->listener_registry->register($listener, fn() => new FakeListener);
        } catch (\Exception $e) {
            $first_exception = $e;
        }

        try {
            $this->listener_registry->register($listener_id, fn() => new FakeListener);
        } catch (\Exception $e) {
            $second_exception = $e;
        }

        $this->assertInstanceOf(ListenerAlreadyRegistered::class, $first_exception);
        $this->assertInstanceOf(ListenerAlreadyRegistered::class, $second_exception);
    }

    /** @test */
    function it_returns_the_ids_of_all_the_listeners_registered_with_it()
    {
        $listener_a_id = new ListenerId('\First\Example\Listener');
        $listener_b_id = new ListenerId('\Second\Example\Listener');

        $this->listener_registry->register(
            $listener_a_id,
            fn() => new ListenerId('\First\Example\Listener'),
            EventA::class,
        );
        $this->listener_registry->register(
            $listener_b_id,
            fn() => new ListenerId('\Second\Example\Listener'),
            EventA::class
        );

        $returned_listener_ids = $this->listener_registry->allIds();

        $this->assertCount(2, $returned_listener_ids);
        $this->assertContainsEquals($listener_a_id, $returned_listener_ids);
        $this->assertContainsEquals($listener_b_id, $returned_listener_ids);
    }
}
