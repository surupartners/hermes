red="$(tput setaf 1)"
reset="$(tput sgr0)"

function arrayContains {
  local -r value="$1"
  local -ar array=( "${@:2}" )

  for item in "${array[@]}"; do
    if [[ "$item" == "$value" ]]; then
      return 0
    fi
  done

  return 1
}

function info {
  local -r source="${command_name:-$0}"
  echo -n "${source}: " 1>&2
  echo -e "$@" 1>&2
}

function error {
  echo -n "$red" 1>&2
  info "$@"
  echo -n "$reset" 1>&2
}
