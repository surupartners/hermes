<?php

return [
    'position_tracker_table_name' => 'hermes_stream_position_markers',
    'stream_processor_states_table_name' => 'hermes_stream_processor_states',
    'stream_item_transformations_registration_path' => [],
    'graceful_termination_timeout_in_seconds' => 10,
    'monitoring_frequency_in_seconds' => 10,
    'recheck_delay_after_end_of_stream_in_seconds' => 10,
    'processing_delay_between_stream_items_in_seconds' => 0,
    'immediate_restart_limit' => 2,
    'immediate_restart_limit_window_in_seconds' => 180,
    'restart_back_off_period_in_hours' => 1,
    'restart_back_off_limit' => 6,
    'restart_back_off_limit_window_in_hours' => 24,
    'stream_processors' => [],
    'streams' => [],
    'event_listeners' => [],
];
