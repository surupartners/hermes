<?php

use Hermes\Stream\StreamItem;

// Register closures used to determine whether a given stream item can be transformed into a given event and to perform
// the transformation. This file must be kept outside the Laravel config directory because closures can't be serialised.
// No need to check the stream that the stream item has come from, that will be done automatically for you.

// Return a list of pairs of callbacks
return [
    // Example transformation
    [
        // A callback which checks whether a stream item can be transformed into the event returned by the
        // transformation callback below.
        'matching_predicate' => function(StreamItem $item) : bool {
            $payload = json_decode($item->payload(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return false;
            }

            return array_key_exists('name', $payload) && $payload['name'] == 'Example Item';
        },
        // A callback which transforms a stream item into a given event.
        // Note: the return type of this callback is important. It must be the name of an event class in your app.
        'transformation' => function(StreamItem $item) : stdClass {
            // You'll transform to an event listened to by your app, not an standard object.
            return new stdClass();
        },
    ]
];
