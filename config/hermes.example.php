<?php

return [
    'position_tracker_table_name' => 'hermes_position_markers',
    'stream_processor_states_table_name' => 'hermes_stream_processor_states',
    // The laravel config caching process can't serialise closures so we put them in a file outside the config directory
    // and provide its path here. You can also provide an array of multiple paths. Later files will have higher
    // precedence than earlier ones.
    'stream_item_transformations_registration_path' => app_path('hermes_stream_item_to_event_transformations.php'),
    'monitoring_frequency_in_seconds' => 10,

    // These values can all be provided globally, per stream, and per stream processor
    'recheck_delay_after_end_of_stream_in_seconds' => 10,
    'processing_delay_between_stream_items_in_seconds' => 0,
    'immediate_restart_limit' => 2,
    'immediate_restart_limit_window_in_seconds' => 180,
    'restart_back_off_period_in_hours' => 1,
    'restart_back_off_limit' => 6,
    'restart_back_off_limit_window_in_hours' => 24,
    'graceful_termination_timeout_in_seconds' => 10,

    // Optional, you can provide stream processor config values for specific stream & listener combinations.
    // These will take precedence over global & stream-specific values.
    'stream_processors' => [[
        'stream' => '\Example\Stream\Implementation',
        'listener' => '\Example\Event\Listener',
        'processing_interval_in_seconds' => 12,
    ]],

    'streams' => [[
        'class' => '\Example\Stream\Implementation', // Must implement \Hermes\Stream\Stream interface.
        // A list of the events which this app is interested in reading off this stream. An event may only be come from
        // one stream.
        'events' => [
            '\Something\Was\Created',
            '\Something\Was\Updated',
        ],
        // Optional, you can provide stream processor config values for processors reading this stream.
        // These will take precedence over global values.
        'processing_interval_in_seconds' => 20,
    ]],

    // Optional, event listeners can be registered dynamically with Hermes\Event\ListenerRegistry
    'event_listeners' => [[
        'class' => '\Example\Event\Listener',
        // A list of the streams from which this event listener cares about the events.
        'listens_for_events_from_streams' => [
            '\Example\Stream\Implementation',
        ],
    ]],
];
